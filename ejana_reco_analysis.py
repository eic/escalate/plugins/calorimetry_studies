# Make sure pyjano is installed in your system
# do:
#     pip install --upgrade pyjano          # for conda, venv or root install
#     pip install --user --upgrade pyjano   # for user local install
#
#
import pyjano
from pyjano.jana import Jana, PluginFromSource
from cal_auto import *

#print(pyjano.__file__)

input_file = 'work/beagle_100000evt.root'
output_file = 'work/beagle_100000evt_cali1.ana.root'

jana = Jana(nevents=100000, output=output_file)
jana.plugin('g4e_reader')
    #.plugin('dis')              # DIS plots

# Ce EMCAL island reconstruction
jana.plugin('islreco', inner_profile='profiles/prof_pwo.dat', outer_profile='profiles/prof_lg.dat', verbose=1)


# Add our analysis plugin (will be auto build from a directory)
emcal_reco_analysis = PluginFromSource('emcal_reco_analysis')   # Name will be determined from folder name
emcal_reco_analysis.builder.config['cxx_standard'] = 17

# Parameters:
#     verbose      - Plugin output level. 0-almost nothing, 1-some, 2-everything
#     only_recoil  - Cut that leaves only truly recoil electrons
# Beams energies. Defaults are 10x100 GeV
#     e_beam_energy    -  Energy of colliding electron beam");
#     ion_beam_energy  -  Energy of colliding ion beam");
jana.plugin(emcal_reco_analysis, verbose=2, e_beam_energy=10, ion_beam_energy=100)

jana.source(input_file)
jana.run()

#event_display_gen(input_file, output_file, "full")
