# A simple script to count a number of events in BeAGLE file
# Use it like
# python3 count_beagle_events_in_file.py <file name>

import sys


def parse_beagle_event_count(file_name):
    bufsize = 1000000
    event_counter = 0

    with open(file_name) as infile:
        while True:
            lines = infile.readlines(bufsize)
            if not lines:
                break
            for i, line in enumerate(lines):
                if line.startswith(" =============== Event f"):
                    event_counter += 1
    return event_counter


if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("Script counts number of events in BeAGLE file")
        print("Provide file name!")
        exit(1)

    print(parse_beagle_event_count(sys.argv[1]))
