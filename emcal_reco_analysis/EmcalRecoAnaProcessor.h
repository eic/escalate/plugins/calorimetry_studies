#ifndef EmcalRecoAnaProcessor_header_
#define EmcalRecoAnaProcessor_header_

#include <thread>
#include <atomic>
#include <mutex>

#include <JANA/JEventProcessor.h>
#include <JANA/JObject.h>
#include <ejana/EServicePool.h>

#include "Math/Point3D.h"
#include "Math/Vector4D.h"
#include "EmcalRecoAnaProcessor.h"
#include "EmcalRecoAnaRootOutput.h"
#include <TLorentzVector.h>
#include <MinimalistModel/McTrack.h>
#include <MinimalistModel/McGeneratedParticle.h>

class JApplication;

struct EmcalStatistics {
    uint64_t events_count;
    uint64_t total_el_in_barrel;
    uint64_t total_el_in_elcap;
    uint64_t recoil_el_in_elcap;
    uint64_t recoil_el_in_ioncap;
    uint64_t el_total_count;
};


class EmcalRecoAnaProcessor : public JEventProcessor
{
  public:

    // Constructor just applies
    explicit EmcalRecoAnaProcessor(JApplication *app=nullptr):
            JEventProcessor(app),
            services(app)
                {};

    // This is called once before the first call to the Process method
    // below. You may, for example, want to open an output file here.
    // Only one thread will call this.
    void Init() override;

    //----------------------------
    // Process
    //
    // This is called for every event. Multiple threads may call this
    // simultaneously. If you write something to an output file here
    // then make sure to protect it with a mutex or similar mechanism.
    // Minimize what is done while locked since that directly affects
    // the multi-threaded performance.
    void Process(const std::shared_ptr<const JEvent>& event) override;


    //----------------------------
    // Finish
    //
    // This is called once after all events have been processed. You may,
    // for example, want to close an output file here.
    // Only one thread will call this.
    void Finish() override;

private:
    EmcalRootOutput root;
    ej::EServicePool services;
    EmcalStatistics _stat;


    // Plugin parameters, can be set from the command line
    int verbose;                // verbose output level
    bool only_recoil;           // Cut that leaves only truly recoil electrons
    double e_beam_energy;       // electron beam energy (for X,Y,Q2 calculation)
    double ion_beam_energy;     // ion beam energy (for X,Y,Q2 calculation)
    double e_calibration;       // Calorimeter calibration constant

    const minimodel::McGeneratedParticle * GetRecoilElectron(const std::shared_ptr<const JEvent> &event);
};

#endif   // EmcalRecoAnaProcessor_header_
