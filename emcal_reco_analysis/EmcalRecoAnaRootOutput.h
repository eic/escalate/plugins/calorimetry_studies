#ifndef EMCAL_ROOT_OUTPUT_HEADER
#define EMCAL_ROOT_OUTPUT_HEADER

#include <TFile.h>
#include <TDirectory.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TCanvas.h>
#include <TTree.h>
#include <TApplication.h>
#include <TLorentzVector.h>

// This structure is used to fill tree with each electron data to build plots later
struct ElectronRecord {
    uint64_t event_index;
    TLorentzVector p;
    bool is_recoil;
    bool is_in_elcap;
    double x_true;
    double x_em;
    double y_true;
    double y_em;
    double q2_true;
    double q2_em;
};

struct ClusterHitRecord {

    double cell_address;
    double cell_global_id;
    double cell_fraction = 0;
    double cell_total_energy = 0;
    double cell_total_adc = 0;
    double   cluster_id;
    double   cluster_energy;    /// In uncalibrated units
    double   cluster_abs_x;
    double   cluster_abs_y;
    double   cluster_rel_x;      /// Coordinate of incidence is given in length between module centers
    double   cluster_rel_y;
    double   cluster_chi2;       /// Chi2
    double   cluster_cluster_size;
    double   cluster_type;
};

struct TrackHitRecord {
    uint64_t id;
    uint64_t pdg;
    uint64_t incidence_x;
    uint64_t incidence_y;
    uint64_t incidence_z;
    uint64_t energy;
    uint64_t vtx_x;
    uint64_t vtx_y;
    uint64_t vtx_z;
};


class EmcalRootOutput
{
public:
    void init(TFile *file)
    {
        double x_max = 0.03;
        double q2_max = 10;

        // 'locker' locks mutex so other threads can't interfere with TFile doing its job
        std::lock_guard<std::recursive_mutex> locker(lock);

        // create a subdirectory "hist_dir" in this file
        plugin_root_dir = file->mkdir("gen_emcal");
        file->cd();         // Just in case some other root file is the main TDirectory now
        plugin_root_dir->cd();

        // TTree with recoiled electron
        tree_rec_e = new TTree("el_tree", "Recoiled electron tree");
        tree_rec_e->SetDirectory(plugin_root_dir);

        tree_rec_e->Branch("evt_id", &el_record().event_index, "evt_id/i");
        tree_rec_e->Branch("p", &el_record().p);
        tree_rec_e->Branch("is_recoil", &el_record().is_recoil);
        tree_rec_e->Branch("is_in_elcap", &el_record().is_in_elcap);
        tree_rec_e->Branch("x_true", &el_record().x_true);
        tree_rec_e->Branch("x_em", &el_record().x_em);
        tree_rec_e->Branch("y_true", &el_record().y_true);
        tree_rec_e->Branch("y_em", &el_record().y_em);
        tree_rec_e->Branch("q2_true", &el_record().q2_true);
        tree_rec_e->Branch("q2_em", &el_record().q2_em);

        tree_cluster_hit = new TTree("cluster_hit_tree", "Clusters hits tree");
        tree_cluster_hit->SetDirectory(plugin_root_dir);

        tree_cluster_hit->Branch("cell_address", &chit_record().cell_address);
        tree_cluster_hit->Branch("cell_global_id", &chit_record().cell_global_id);
        tree_cluster_hit->Branch("cell_fraction", &chit_record().cell_fraction);
        tree_cluster_hit->Branch("cell_total_energy", &chit_record().cell_total_energy);
        tree_cluster_hit->Branch("cell_total_adc", &chit_record().cell_total_adc);
        tree_cluster_hit->Branch("cluster_id", &chit_record().cluster_id);
        tree_cluster_hit->Branch("cluster_energy", &chit_record().cluster_energy);
        tree_cluster_hit->Branch("cluster_abs_x", &chit_record().cluster_abs_x);
        tree_cluster_hit->Branch("cluster_abs_y", &chit_record().cluster_abs_y);
        tree_cluster_hit->Branch("cluster_rel_x", &chit_record().cluster_rel_x);
        tree_cluster_hit->Branch("cluster_rel_y", &chit_record().cluster_rel_y);
        tree_cluster_hit->Branch("cluster_chi2", &chit_record().cluster_chi2);
        tree_cluster_hit->Branch("cluster_cluster_size", &chit_record().cluster_cluster_size);
        tree_cluster_hit->Branch("cluster_type", &chit_record().cluster_type);

        /*tree_mc_track = new TTree("mc_track_tree", "Tracks and some hits information from MC");
        tree_mc_track->SetDirectory(plugin_root_dir);*/

        //=============================================================================================
        //------------------- electrons -------------------------------

        h1_all_el_e_tot = new TH1D("all_el_e_tot", "Energy of all electrons from primary vtx", 500, 0., 30.);
        h1_all_el_e_tot->GetXaxis()->SetTitle("E [GeV]");

        h1_all_el_theta = new TH1D("all_el_theta", " Theta of all electrons from primary vtx", 100, 0., 180.);
        h1_all_el_theta->GetXaxis()->SetTitle("Theta [deg]");

        h1_el_e_tot = new TH1D("rec_el_e_tot", "Energy of the recoil electron", 500, 0., 15.);
        h1_el_e_tot->GetXaxis()->SetTitle("E [GeV]");

        h1_el_theta = new TH1D("rec_el_theta", " Theta of the recoil electron", 100, 0., 180.);
        h1_el_theta->GetXaxis()->SetTitle("Theta [deg]");

        h1_empz = new TH1F("el_empz" , " E- Pz electron " , 100, 0., 50.   );

        h1_all_el_num = new TH1I("all_el_num", "Number of electrons in an event", 21, 0, 20);
        h1_all_el_num->GetXaxis()->SetTitle("Number of electrons");

        h1_emcal_el_num = new TH1I("emcal_el_num", "Number of electrons that hit any EMCAL", 21, 0, 20);
        h1_emcal_el_num->GetXaxis()->SetTitle("Number of electrons");

        h1_elcap_el_num = new TH1I("elcap_el_num", "Number of electrons in electron cap EMCAL", 21, 0, 20);
        h1_elcap_el_num->GetXaxis()->SetTitle("Number of electrons");

        //=====================================================================
        //------------------- Kinematics X,Q2 -------------------------------

        // Bins and edges for logarithmic x and q2 particles histograms
        const int x_bins=19;
        double x_edges[x_bins + 1] =
                {-3.8, -3.6, -3.4, -3.2, -3.,
                 -2.8, -2.6, -2.4, -2.2, -2.,
                 -1.8, -1.6, -1.4, -1.2, -1.,
                 -0.8, -0.6, -0.4, -0.2, 0.};

        const int q2_bins=16;
        double q2_edges[q2_bins + 1] =
                {0.,   0.25,  0.5,  0.75, 1.,
                 1.25, 1.5,   1.75, 2.,   2.25,
                 2.5,  2.75,  3.,   3.25, 3.5,
                 3.75, 4.};

        // True values
        h2_xq2_true = new TH2I("XQ2_true", " True values Q^{2} vs X ", 100, 0., x_max, 100, 0, q2_max);
        h2_xq2_true->GetXaxis()->SetTitle("x_{true}");
        h2_xq2_true->GetYaxis()->SetTitle("Q^{2}_{true}");

        h2_xq2_true_log = new TH2I("XQ2_true_log", "True values Q^{2} vs X log", x_bins, x_edges, q2_bins, q2_edges);
        h2_xq2_true_log->GetXaxis()->SetTitle("log10(x_{em})");
        h2_xq2_true_log->GetYaxis()->SetTitle("log10(Q^{2}_{em})");

        // Electron method
        h2_xq2_em = new TH2I("XQ2_em", " EM Q^{2} vs X", 100, 0., x_max, 100, 0, q2_max);
        h2_xq2_em->GetXaxis()->SetTitle("x_{em}");
        h2_xq2_em->GetYaxis()->SetTitle("Q^{2}_{em}");

        h2_xq2_em_log = new TH2I("XQ2_em_log", "EM Q^{2} vs X log ", x_bins, x_edges, q2_bins, q2_edges);
        h2_xq2_em_log->GetXaxis()->SetTitle("log10(x_{em})");
        h2_xq2_em_log->GetYaxis()->SetTitle("log10(Q^{2}_{em})");

        h2_q2em_vs_true = new TH2D("q2em_vs_true", "Q^{2}_{em} vs Q^{2}_{true}", 100, 0., q2_max, 100, 0., q2_max);
        h2_q2em_vs_true->GetXaxis()->SetTitle("Q^{2}_{true}");
        h2_q2em_vs_true->GetYaxis()->SetTitle("Q^{2}_{true}-Q2_{em}");

        h2_q2em_vs_true_log = new TH2D("q2em_vs_true_log", "log10(Q^{2}_{em}) vs log10(Q^{2}_{true})", q2_bins, q2_edges, q2_bins, q2_edges);
        h2_q2em_vs_true_log->GetXaxis()->SetTitle("log10(Q^{2}_{true})");
        h2_q2em_vs_true_log->GetYaxis()->SetTitle("log10(Q^{2}_{em})");

        h2_q2diff_vs_true = new TH2D("q2diff_vs_true", "(Q^{2}_{true}-Q^{2}_{em})/Q^{2}_{true} vs Q^{2}_{true}", 100, 0., q2_max, 100, -0.1, 0.1);
        h2_q2diff_vs_true->GetXaxis()->SetTitle("Q^{2}_{true}");
        h2_q2diff_vs_true->GetYaxis()->SetTitle("(Q^{2}_{true}-Q^{2}_{em})/Q^{2}_{true}");

        h2_q2diff_vs_true_log = new TH2D("q2diff_vs_true_log", "(Q^{2}_{true}-Q^{2}_{em})/Q^{2}_{true} vs Q^{2}_{true}", 100, -1, 2, 100, -1., 1);
        h2_q2diff_vs_true_log->GetXaxis()->SetTitle("log10(Q^{2}_{true})");
        h2_q2diff_vs_true_log->GetYaxis()->SetTitle("(Q^{2}_{true}-Q^{2}_{em})/Q^{2}_{true}");

        h2_xdiff_vs_true = new TH2D("xdiff_vs_true", "(X_{true}-X_{em})/X_{true} vs X_{true}", 100, 0., x_max, 100, -0.1, 0.2);
        h2_xdiff_vs_true->GetXaxis()->SetTitle("X_{true}");
        h2_xdiff_vs_true->GetYaxis()->SetTitle("(X_{true}-X_{em})/X_{true}");

        h2_xdiff_vs_true_log = new TH2D("xdiff_vs_true_log", "(X_{true}-X_{em})/X_{true} vs X_{true}", 100, -1, 2, 100, -1., 1);
        h2_xdiff_vs_true_log->GetXaxis()->SetTitle("log10(X_{true})");
        h2_xdiff_vs_true_log->GetYaxis()->SetTitle("(X_{true}-X_{em})/X_{true}");

        h1_dis_x_em = new TH1D("X_em", "X from e-method", 100, 0., x_max);
        h1_dis_x_em->GetXaxis()->SetTitle("X_{em}");

        h1_dis_x_em_log = new TH1D("X_em_log", "X from e-method (log)", x_bins, x_edges);
        h1_dis_x_em_log->GetXaxis()->SetTitle("X_{em}");

        h1_dis_y_em = new TH1D("Y_true", "True Y ", 100, 0., 1.);
        h1_dis_y_em->GetXaxis()->SetTitle("y_{true}");

        // Hits
        h2_xy_hits_elcap = new TH2I("xy_hits_elcap", "X,Y hits in Electron Endcap EMCAL", 200, -1500, 1500, 200, -1500, 1500);

        // RECO
        h1_reco_energy = new TH1F("reco_e_tot", "Energy of the cal reconstructed particles", 500, 0., 30.);
        h1_el_e_tot->GetXaxis()->SetTitle("E [GeV]");

        h2_e_truereco_vs_e = new TH2I("e_truereco_vs_e", "E_{em}/E_{true} vs E", 100, 0., 10, 100, 0., 1.5);
        h2_e_truereco_vs_e->GetXaxis()->SetTitle("E");
        h2_e_truereco_vs_e->GetYaxis()->SetTitle("E_{em}/E_{true}");

        h2_e_truereco = new TH1D("e_truereco", "E_{em}/E_{true}", 600, 0., 1.1);
        h2_e_truereco->GetXaxis()->SetTitle("E_{em}/E_{true}");

        h2_x_truereco_vs_x = new TH2I("x_truereco_vs_x", "X_{em}/X_{true} vs X", 100, 0., .2, 100, -.05, 2);
        h2_x_truereco_vs_x->GetXaxis()->SetTitle("X");
        h2_x_truereco_vs_x->GetYaxis()->SetTitle("X_{em}/X_{true}");

        h2_x_truereco = new TH1D("x_truereco", "X_{em}/X_{true}", 600, 0., 1.1);
        h2_x_truereco->GetXaxis()->SetTitle("X_{em}/X_{true}");

        h2_q2_truereco_vs_q2 = new TH2I("q2_truereco_vs_q2", "Q^{2}_{em}/Q^{2}_{true} vs Q^{2}_{true}", 100, 0., 10, 100, 0., 1.5);
        h2_q2_truereco_vs_q2->GetXaxis()->SetTitle("Q^{2}");
        h2_q2_truereco_vs_q2->GetYaxis()->SetTitle("Q^{2}_{em}/Q^{2}_{true}");

        h2_q2_truereco = new TH1D("q2_truereco", "Q^{2}_{em}/Q^{2}_{true}", 600, 0., 1.1);
        h2_q2_truereco->GetXaxis()->SetTitle("Q^{2}_{em}/Q^{2}_{true}");
    }

    /// This function is a trick to initialize static in header only class
    ElectronRecord& el_record() {
        static ElectronRecord el_record;    // This is used to build a tree
        return el_record;
    }

    /// This function is a trick to initialize static in header only class
    ClusterHitRecord& chit_record() {
        static ClusterHitRecord cluster_hit_record;    // This is used to build a tree
        return cluster_hit_record;
    }

    // ---- hits ----
    TH2I *h2_xy_hits_elcap;     // X,Y plots for Electron endcap EMCAL

    // ---- reco hits ----
    TH1F *h1_reco_energy;
    TH1F *h1_x_energy;


    // ---- kinematic variables---
    TH2I *h2_xq2_true;
    TH2I *h2_xq2_true_log;
    TH2I *h2_xq2_em;
    TH2I *h2_xq2_em_log;
    TH1D *h1_dis_x_em;
    TH1D *h1_dis_x_em_log;
    TH1D *h1_dis_y_em;
    TH2I *h2_e_truereco_vs_e;
    TH1D *h2_e_truereco;
    TH2I *h2_q2_truereco_vs_q2;
    TH1D *h2_q2_truereco;
    TH2I *h2_x_truereco_vs_x;
    TH1D *h2_x_truereco;


    TH2D *h2_q2em_vs_true;
    TH2D *h2_q2em_vs_true_log;
    TH2D *h2_q2diff_vs_true;
    TH2D *h2_q2diff_vs_true_log;
    TH2D *h2_xdiff_vs_true;
    TH2D *h2_xdiff_vs_true_log;

    TH1F *h1_empz;

    // ---- Electron ----
    TH1D *h1_all_el_theta;
    TH1D *h1_all_el_e_tot;
    TH1D *h1_el_e_tot;
    TH1D *h1_el_theta;
    TH1I *h1_all_el_num;
    TH1I *h1_emcal_el_num;
    TH1I *h1_elcap_el_num;

    std::recursive_mutex lock;

    TTree * tree_rec_e;     // Tree to store electron related data
    TTree * tree_cluster_hit;     // Tree to store electron related data
    TTree * tree_mc_track;     // Tree to store electron related data

private:

    TDirectory* plugin_root_dir;   // Main TDirectory for Plugin histograms and data
};

#endif // OPEN_CHARM_ROOT_OUTPUT_HEADER
