#include <JANA/JApplication.h>

#include "EmcalRecoAnaProcessor.h"

extern "C"
{
    void InitPlugin(JApplication *app)
    {
        InitJANAPlugin(app);

        app->Add(new EmcalRecoAnaProcessor(app));
    }
}
