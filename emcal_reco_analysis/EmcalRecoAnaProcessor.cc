#include "TLorentzVector.h"

#include <fmt/core.h>

#include <JANA/JEvent.h>

#include <dis/functions.h>
#include <MinimalistModel/McFluxHit.h>
#include <MinimalistModel/McGeneratedParticle.h>
#include <MinimalistModel/McTrack.h>
#include <MinimalistModel/DisInfo.h>

#include "EmcalRecoAnaProcessor.h"

// #include <TClingRuntime.h>
#include <fmt/format.h>             // For format and print functions
#include <ejana/EServicePool.h>
#include <ejana/plugins/reco/islreco/CeEmcalCluster.h>

// TODO repalce by some 'conventional' PDG info provider. Like TDatabasePDG
//==================================================================
Double_t MassPI = 0.139570,
        MassK = 0.493677,
        MassProton = 0.93827,
        MassNeutron = 0.939565,
        MassE = 0.00051099895,
        MassMU = 0.105658;
//==================================================================

using namespace std;
using namespace fmt;


void EmcalRecoAnaProcessor::Init() {
    ///  Called once at program start.
    print("GeneralEmcalAnalysis::Init()\n");

    // initialize class that holds histograms and other root objects
    // 'services' is a service locator, we ask it for TFile
    root.init(services.Get<TFile>());

    // Ask service locator for parameter manager. We want to get this plugin parameters.
    // JParameterManager allows to set parameters from command line or python with
    // -Pplugin_name:param_name=value  flags
    auto pm = services.Get<JParameterManager>();

    // Verbosity level. 0 = show nothing. 1 - show some. 2 - mad printer
    // SetDefaultParameter actually sets the parameter value from arguments if it is specified
    verbose = 0;
    pm->SetDefaultParameter("emcal_reco_analysis:verbose", verbose,
                            "Plugin output level. 0-almost nothing, 1-some, 2-everything");

    // Cut that leaves only truly recoil electrons
    only_recoil = true;
    pm->SetDefaultParameter("emcal_reco_analysis:only_recoil", only_recoil,
                            "Cut that leaves only truly recoil electrons");

    // electron beam energy GeV (for X,Y,Q2 calculation)
    e_beam_energy=10;
    pm->SetDefaultParameter("emcal_reco_analysis:e_beam_energy", e_beam_energy,
                            "Electron beam energy [GeV] (for X,Y,Q2 calculation)");

    // ion beam energy GeV (for X,Y,Q2 calculation)
    ion_beam_energy = 100;
    pm->SetDefaultParameter("emcal_reco_analysis:ion_beam_energy", ion_beam_energy,
                            "Ion beam energy [GeV] (for X,Y,Q2 calculation)");

    // ECal energy calibration constant
    //.9659 cali2
    // cali3
    e_calibration = 1;
    //.9659;
    pm->SetDefaultParameter("emcal_reco_analysis:e_calib", e_calibration, "ECal energy calibration constant");

    // Zero total statistics
    _stat.events_count = 0;
    _stat.total_el_in_barrel = 0;
    _stat.total_el_in_elcap = 0;
    _stat.recoil_el_in_elcap = 0;
    _stat.recoil_el_in_ioncap = 0;
    _stat.el_total_count = 0;

    // print out the parameters
    if (verbose) {
        print("Parameters:\n");
        print("  emcal_reco_analysis:verbose         = {0}\n", verbose);
        print("  emcal_reco_analysis:only_recoil     = {0}\n", only_recoil);
        print("  emcal_reco_analysis:e_beam_energy   = {0}\n", e_beam_energy);
        print("  emcal_reco_analysis:ion_beam_energy = {0}\n", ion_beam_energy);
    }
}

const minimodel::McGeneratedParticle * EmcalRecoAnaProcessor::GetRecoilElectron(const std::shared_ptr<const JEvent> &event) {
    /// Returns

    // In beagle the first electron is always the recoil one
    auto gen_parts = event->Get<minimodel::McGeneratedParticle>();
    for(auto particle: gen_parts) {
        if(particle->is_stable && particle->pdg == 11 && particle->charge < 0) {
            return particle;
        }
    }
    // not found
    return nullptr;
}

void EmcalRecoAnaProcessor::Process(const std::shared_ptr<const JEvent> &event) {
    ///< Called every event.
    using namespace fmt;
    using namespace std;
    using namespace minimodel;

    // std::lock_guard<std::recursive_mutex> locker(root_out.lock);
    _stat.events_count++;

    // >oO debug printing
    if ( (event->GetEventNumber() % 1000 == 0 && verbose > 1) || verbose >= 2) {
        print("\n--------- EVENT {} --------- \n", event->GetEventNumber());
    }
    auto tracks = event->Get<minimodel::McTrack>();
    auto hits = event->Get<minimodel::McFluxHit>();
    std::unordered_set<uint64_t> track_ids_in_emcal;
    std:map<uint64_t, const minimodel::McFluxHit*> first_emcal_hits_by_trk_id;
    fmt::print("tracks.count = {}, hits.count = {} \n", tracks.size(), hits.size());

    // Do we have true DIS info ?
    double x_true  = 0;
    double y_true  = 0;
    double q2_true = 0;
    if(event->GetFactory<minimodel::DisInfo>()) {
        auto dis_info = event->GetSingle<minimodel::DisInfo>();
        x_true =  dis_info->x;
        print("x_true = {}\n", x_true);
        y_true =  dis_info->y;
        print("y_true = {}\n", y_true);
        q2_true = dis_info->q2;
        print("q2_true = {}\n", q2_true);

        root.h2_xq2_true->Fill(x_true, q2_true);
        root.h2_xq2_true_log->Fill(log10(x_true), log10(q2_true));
    }
    auto gen_recoil_el = GetRecoilElectron(event);

    if(verbose >= 2) {
        if(gen_recoil_el) {
            fmt::print("particle->trk_id = {}\n", gen_recoil_el->trk_id);
        } else {
            fmt::print("No recoil electron found\n");
        }
    }

    if(gen_recoil_el) {
        root.h1_el_e_tot->Fill(gen_recoil_el->tot_e);
    }

    // Loop over hits
    for (auto hit: hits) {
        // Look at hits at electron EMCAL
        if (ej::StartsWith(hit->vol_name, "ce_EMCAL")) {
            if(!track_ids_in_emcal.count(hit->track_id)) {
                // First track here
                track_ids_in_emcal.insert(hit->track_id);           // Save the track id for further track processing
                first_emcal_hits_by_trk_id[hit->track_id] = hit;    // Save this hit
            }
            // Fill some histograms
            root.h2_xy_hits_elcap->Fill(hit->x, hit->y);
        }
    }

    // Count electrons
    int el_count = 0;           // Count of electrons in event
    int el_emcal_count = 0;     // Count of electrons that got to any emcal
    int el_elcap_count = 0;     // Count of electrons in Electron Endcap EMCAL


    // LOOP OVER TRACKS
    const minimodel::McFluxHit* incidence_hit = nullptr;
    TLorentzVector incidence_track;
    for(auto track: tracks) {
        // CUTS:
        if (track->pdg != 11) continue;       // Take only electrons for now
        if (track->parent_id != 0) continue;  // Take only particles from a generator

        TLorentzVector lv;
        
        lv.SetXYZM(track->px, track->py, track->pz, MassE);
        el_count++;
        _stat.el_total_count++;
        // Fill histograms for all electrons
        //root.h1_all_el_e_tot->Fill(lv.E());
        root.h1_all_el_e_tot->Fill(lv.E()); // Include factor
        root.h1_all_el_theta->Fill(lv.Theta()*57.2958);
    }

//    // Acquire any results you need for your analysis
//
    auto clusters = event->Get<CeEmcalCluster>();
//    // Go through particles
    for(auto& cluster: clusters) {

        if(std::fabs(cluster->energy) < 1e-5) continue;
        if(std::fabs(cluster->chi2) < 0.1) continue;
        if(std::fabs(cluster->chi2) > 2) continue;

        if(verbose >=2) {
            print("Reco particles in EMCAL: id {:<5} clus_size: {:<5} E: {:<11} chi2: {:<11} x: {:<11} y: {}\n",
                  cluster->id, cluster->cluster_size, cluster->energy, cluster->chi2, cluster->abs_x, cluster->abs_y);
        }

        // Fill clusters hits tree
        //print(cell.address);
        for(auto& cell: cluster->cells) {
            root.chit_record().cell_address          = cell.address;
            //print("{}\n",cell.address);
            root.chit_record().cell_global_id        = cell.cell_global_id;
            //print("{}\n",cell.cell_global_id);
            root.chit_record().cell_fraction         = cell.cell_fraction;
            root.chit_record().cell_total_energy     = cell.cell_total_energy;
            root.chit_record().cell_total_adc        = cell.cell_total_adc;
            root.chit_record().cluster_id            = cluster->id;
            root.chit_record().cluster_energy        = cluster->energy;
            root.chit_record().cluster_abs_x         = cluster->abs_x;
            root.chit_record().cluster_abs_y         = cluster->abs_y;
            root.chit_record().cluster_rel_x         = cluster->rel_x;
            root.chit_record().cluster_rel_y         = cluster->rel_y;
            root.chit_record().cluster_chi2          = cluster->chi2;
            root.chit_record().cluster_cluster_size  = cluster->cluster_size;
            root.chit_record().cluster_type          = cluster->type;
            
        }

        /*
        if(section=='pwo_pure'):
            if(max_col<8 or max_col>67 or max_row<8 or max_row>67): # outside edge
                pass
            elif(max_col<48 and max_col>27 and max_row<48 and max_row>27): # inside edge
                pass
            else:
                keep

        if(section=='pwo_edge'):
            if(max_col>8 and max_col<67 and max_row>8 and max_row<67): # outside edge
                pass
            else:
                keep
        */

        root.tree_cluster_hit->Fill();

        root.h1_reco_energy->Fill(cluster->energy);

        if(gen_recoil_el) {

            // Apply calibration coefficient
            double correct_energy = cluster->energy / e_calibration; 
            // calibration coefficient comes from the calibration notebook

            // True scattered electron 4vector
            TLorentzVector lv;
            lv.SetXYZM(gen_recoil_el->px, gen_recoil_el->py, gen_recoil_el->pz, MassE);
            double empz_el = lv.E() - lv.Pz();

            // Calculate x and Q2
            double x_em, y_em, q2_em;
            dis::GEN_El_XYQ2(correct_energy, lv.Theta(), e_beam_energy, ion_beam_energy, x_em, y_em, q2_em);

            //double x_gcem, y_gcem, q2_gcem;
            //dis::GEN_El_XYQ2(lv.E(), lv.Theta(), e_beam_energy, ion_beam_energy, x_em, y_em, q2_em);

            // fill histograms
            root.h2_xq2_em_log->Fill(log10(x_em), log10(q2_em));
            root.h2_xq2_em->Fill(x_em, q2_em);
            root.h1_dis_x_em->Fill(x_em);
            root.h1_dis_x_em_log->Fill(x_em);
            root.h1_dis_y_em->Fill(y_em);
            root.h1_empz->Fill(empz_el);
            root.h1_el_e_tot->Fill(lv.Energy());
            root.h1_el_theta->Fill(lv.Theta()*57.2958);
            root.h2_q2em_vs_true->Fill(q2_true, q2_em);
            root.h2_q2em_vs_true->Fill(log10(q2_true), log10(q2_em));

            root.h2_e_truereco_vs_e->Fill(lv.E(), correct_energy/lv.E());
            root.h2_e_truereco->Fill(correct_energy/lv.E());

            root.h2_q2_truereco_vs_q2->Fill(q2_true, q2_em/q2_true);
            root.h2_q2_truereco->Fill(q2_em/q2_true);

            root.h2_x_truereco_vs_x->Fill(x_true, x_em/x_true);
            root.h2_x_truereco->Fill(x_em/x_true);

            root.h2_q2diff_vs_true->Fill(q2_true, (q2_true - q2_em)/q2_true);
            root.h2_q2diff_vs_true_log->Fill(log10(q2_true), (q2_true - q2_em)/q2_true);

            root.h2_xdiff_vs_true->Fill(x_true, (x_true - x_em)/x_true);
            root.h2_xdiff_vs_true_log->Fill(log10(x_true), (x_true - x_em)/x_true);

            root.h2_q2em_vs_true->Fill(q2_true, q2_em);
            root.h2_q2em_vs_true_log->Fill(log10(q2_true), log10(q2_em));

            // Fill results tree
            // calculate X, Y and Q2 with electron method

            // fill tree for all electrons, recoil or not and for all EMCALs
            root.el_record().event_index = event->GetEventNumber();
            root.el_record().p.SetXYZM(gen_recoil_el->px, gen_recoil_el->py, gen_recoil_el->pz, MassE);
            root.el_record().is_recoil = false;
            root.el_record().is_in_elcap = false;
            root.el_record().x_true = x_true;
            root.el_record().x_em = x_em;
            root.el_record().y_true = y_true;
            root.el_record().y_em = y_em;
            root.el_record().q2_true = q2_true;
            root.el_record().q2_em = q2_em;
            root.tree_rec_e->Fill();
        }
    }

    if(verbose >= 2 && incidence_hit) {
        print("Incidence hit: x={} y={} z={}\n", incidence_hit->x, incidence_hit->y, incidence_hit->z);
        print("Incidence track: E={}\n", incidence_track.E());
    }

    // Fill counts per event histos
    root.h1_emcal_el_num->Fill(el_emcal_count);
    root.h1_all_el_num->Fill(el_count);
    root.h1_elcap_el_num->Fill(el_elcap_count);
}


void EmcalRecoAnaProcessor::Finish() {
    ///< Called after last event of last event source has been processed.
    print("EmcalAnalysis::Finish(). Statistics:\n");
    print("  events_count         : {} \n", _stat.events_count );
    print("  total  el in barrel  : {} \n", _stat.total_el_in_barrel );
    print("  total  el in elcap   : {} \n", _stat.total_el_in_elcap  );
    print("  recoil el in elcap   : {} \n", _stat.recoil_el_in_elcap  );
    print("  recoil el in ioncap  : {} \n", _stat.recoil_el_in_ioncap );
    print("  electrons total      : {} \n", _stat.el_total_count     );
}


