import uproot4
import matplotlib.pyplot as plt
import numpy as np
import pyroot
import ROOT

file = ROOT.TFile('../work/test_reco_analysis.ana.root') 
gen_emcal = file.Get('gen_emcal')
dis_plots = file.Get('dis_plots')
tree = gen_emcal.Get('el_tree')

### --- Q2 vs X true
c1 = ROOT.TCanvas( 'c1', 'The Fit Canvas', 200, 10, 700, 500 )
gen_emcal.Get('XQ2_true').Draw('col')
c1.SaveAs("Plots_Reco/XQ2_true.png")

### Q2 vs. X em
c1 = ROOT.TCanvas( 'c1', 'The Fit Canvas', 200, 10, 700, 500 )
gen_emcal.Get('XQ2_em').Draw('col')
c1.SaveAs("Plots_Reco/XQ2_em.png")


### Q2 em vs. Q2 true
c1 = ROOT.TCanvas( 'c1', 'The Fit Canvas', 200, 10, 700, 500 )
gen_emcal.Get('q2em_vs_true').Draw('col')
c1.SaveAs("Plots_Reco/q2em_vs_true.png")

### Q2 diff vs. Q2 true
c1 = ROOT.TCanvas( 'c1', 'The Fit Canvas', 200, 10, 700, 500 )
gen_emcal.Get('q2diff_vs_true').Draw('col')
c1.SaveAs("Plots_Reco/q2diff_vs_true.png")

### Reco
c1 = ROOT.TCanvas( 'c1', 'The Fit Canvas', 200, 10, 700, 500 )
gen_emcal.Get('reco_e_tot').Draw('col')
c1.SaveAs("Plots_Reco/reco_e_tot.png")

### e_truereco_vs_e
c1 = ROOT.TCanvas( 'c1', 'The Fit Canvas', 200, 10, 700, 500 )
gen_emcal.Get('e_truereco_vs_e').Draw('col')
c1.SaveAs("Plots_Reco/e_truereco_vs_e.png")

### e_truereco
c1 = ROOT.TCanvas( 'c1', 'The Fit Canvas', 200, 10, 700, 500 )
gen_emcal.Get('e_truereco').Draw('col')
c1.SaveAs("Plots_Reco/e_truereco.png")