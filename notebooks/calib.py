import uproot4
#from ROOT import TH1F
from hist import Hist
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.patches as patches
from matplotlib.collections import PatchCollection
from matplotlib import cm
from matplotlib.colors import LogNorm, Normalize
from uncertainties import unumpy as unp
import numpy as np
import awkward1 as ak
from scipy.optimize import curve_fit
from scipy.stats import crystalball
from scipy.special import erf
from zfit.models.physics import crystalball_func
import math


mpl.rcParams['text.usetex'] = True
plt.rc('text', usetex=False)


'''MY CRYSTAL BALL FUNCTION'''
#[1, 1, 4.81, .05, max(reco_e[0])])#alpha, n, mean, sigma, amp
def crystalball(x, alpha, n, mean, sigma, N):#N=amp
    #alpha=1
    #n=1.999
    #mean=4.81
    N=max(reco_e[0])
    func = np.array([])
    #print(n)
    #print(x)
    for x in x:
        A = np.float_power(n/abs(alpha),n)*np.exp(-1*(alpha**2)/2)
        B = (n/abs(alpha))-abs(alpha)
        C = (n/abs(alpha))*(np.exp(-1*(alpha**2)/2))/(n-1)
        D = np.sqrt(np.pi/2)*(1+erf(abs(alpha)/np.sqrt(2)))
        #N=1/(sigma*(C+D))
        if((x-mean)/sigma > -1*alpha):
            func = np.append(func, [N*np.exp(-1*((x-mean)**2)/(2*(sigma**2)))])
        elif((x-mean)/sigma <= -1*alpha):
            func = np.append(func, [N*A*((np.float_power((B-(x-mean)/sigma),(-1*n))))])
    return func

### --- https://stackoverflow.com/questions/2566412/find-nearest-value-in-numpy-array
def find_nearest(array,value):
    idx,val = min(enumerate(array), key=lambda x: abs(x[1]-value))
    return idx

act_energy = 5 #GeV
ana_file = uproot4.open(f"../work/calib_gam_{act_energy}GeV_100000evt.ana.root")
reco_e = ana_file['ce_emcal_calib;1/reco_energy;1'].to_numpy()
reco_chi2 = ana_file['ce_emcal_calib;1/reco_chi2;1'].to_numpy()

### reduce x range to where energy is
bin_centers = reco_e[1][:-1] + np.diff(reco_e[1]) / 2
energy = np.argmax(reco_e[0])
e_reco = reco_e[1][energy]
e_min = e_reco-.3
e_max = e_reco+.3
arg_min = find_nearest(reco_e[1], e_min)#; arg_min = arg_min[0][0]
arg_max = find_nearest(reco_e[1], e_max)#; arg_max = arg_max[0][0]

fig, ax = plt.subplots()
### plot histogram
width = .85*(reco_e[1][1] - reco_e[1][0])
plt.bar(reco_e[1][arg_min:arg_max], reco_e[0][arg_min:arg_max], align='edge', width=width)

### plot fit
beta, m, scale = act_energy, 3, .4
x_interval_for_fit = np.linspace(reco_e[1][arg_min], reco_e[1][arg_max], 10000)
popt, _ = curve_fit(crystalball, bin_centers[arg_min:arg_max], reco_e[0][arg_min:arg_max], p0=[1, 1, 4.81, .05, max(reco_e[0])])#alpha, n, mean, sigma, amp
print(popt)
fit = crystalball(x_interval_for_fit, *popt)#, 1, 1, 4.81, .05, max(reco_e[0]))#alpha, n, mean, sigma, amp
ax.plot(x_interval_for_fit, fit, label='fit', color="orange")
mean = popt[2]
std = popt[3]

# place a text box in upper left in axes coords
textstr = '\n'.join((r'$\mu=%.2f$' % (round(mean,2), ), r'$\sigma=%.2f$' % (round(std, 2), )))
props = dict(boxstyle='round', facecolor='wheat', alpha=0.5)
ax.text(0.05, 0.95, textstr, transform=ax.transAxes, fontsize=14, verticalalignment='top', bbox=props)

ax.legend()
ax.set_xlabel("Energy")
ax.set_ylabel("Events")
plt.show()