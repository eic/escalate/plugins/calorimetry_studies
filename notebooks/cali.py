# To add a new cell, type '# %%'
# To add a new markdown cell, type '# %% [markdown]'
# %%
from IPython import get_ipython

# %%
get_ipython().run_line_magic('matplotlib', 'inline')
import uproot4
#from ROOT import TH1F
from hist import Hist
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.patches as patches
from matplotlib.collections import PatchCollection
from matplotlib import cm
from matplotlib.colors import LogNorm, Normalize
from uncertainties import unumpy as unp
import numpy as np
import awkward1 as ak
from scipy.optimize import curve_fit
from scipy.stats import crystalball
from scipy.special import erf
from zfit.models.physics import crystalball_func
import math


mpl.rcParams['text.usetex'] = True
plt.rc('text', usetex=False)


# %%
# Open file and get hits in events
root_file = uproot4.open("../work/calib_gam_5GeV_100000evt.root")
ana_file = uproot4.open("../work/calib_gam_5GeV_100000evt.ana.root")
nevents = 300
ana_files = [
uproot4.open('../work/calib_gam_1GeV_100000evt.ana.root'),
uproot4.open('../work/calib_gam_3GeV_100000evt.ana.root'),
uproot4.open('../work/calib_gam_5GeV_100000evt.ana.root'),
uproot4.open('../work/calib_gam_7GeV_100000evt.ana.root'),
uproot4.open('../work/calib_gam_9GeV_100000evt.ana.root')
]


def pdf(x, a=1 / np.sqrt(2 * np.pi), x0=0, sigma=1, offset=0):
    exp = unp.exp if a.dtype == np.dtype("O") else np.exp
    return a * exp(-((x - x0) ** 2) / (2 * sigma ** 2)) + offset


# %%
'''GAUSSIAN FUNCTION'''

### --- https://stackoverflow.com/questions/2566412/find-nearest-value-in-numpy-array
def find_nearest(array,value):
    idx,val = min(enumerate(array), key=lambda x: abs(x[1]-value))
    return idx

### --- https://stackoverflow.com/questions/35544233/fit-a-curve-to-a-histogram-in-python
def gaussian(x, mean, amplitude, standard_deviation):
    return amplitude * np.exp( - (x - mean)**2 / (2*standard_deviation ** 2))

act_energy = 5 #GeV
ana_file = uproot4.open(f"../work/calib_gam_{act_energy}GeV_100000evt.ana.root")
reco_e = ana_file['ce_emcal_calib;1/reco_energy;1'].to_numpy()
reco_chi2 = ana_file['ce_emcal_calib;1/reco_chi2;1'].to_numpy()

### reduce x range to where energy is
bin_centers = reco_e[1][:-1] + np.diff(reco_e[1]) / 2
energy = np.argmax(reco_e[0])
e_min = reco_e[1][energy]-.3
e_max = reco_e[1][energy]+.3
arg_min = find_nearest(reco_e[1], e_min)#; arg_min = arg_min[0][0]
arg_max = find_nearest(reco_e[1], e_max)#; arg_max = arg_max[0][0]

x_interval_for_fit = np.linspace(reco_e[1][arg_min], reco_e[1][arg_max], 10000)
popt, _ = curve_fit(gaussian, reco_e[1][arg_min:arg_max], bin_centers[arg_min:arg_max], p0=[1., 0., act_energy])
gauss = gaussian(x_interval_for_fit, *popt)
gauss_mean = np.mean(gauss)
gauss_std = np.std(gauss)
print("mean: ", round(gauss_mean,2))
print("std:  ", round(gauss_std, 2))

### plot fit
popt, _ = curve_fit(gaussian, bin_centers[arg_min:arg_max], reco_e[0][arg_min:arg_max], p0=[act_energy,max(reco_e[0]),1.])#mean, amplitude, standard_deviation
gauss = gaussian(x_interval_for_fit, *popt)
print(popt)
fig, ax = plt.subplots()
### plot histogram
width = .85*(reco_e[1][1] - reco_e[1][0])
plt.bar(reco_e[1][arg_min:arg_max], reco_e[0][arg_min:arg_max], align='edge', width=width)
textstr = '\n'.join((
    r'$\mu=%.2f$' % (round(gauss_mean,2), ),
    r'$\sigma=%.2f$' % (round(gauss_std, 2), )))


# these are matplotlib.patch.Patch properties
props = dict(boxstyle='round', facecolor='wheat', alpha=0.5)
# place a text box in upper left in axes coords
ax.text(0.05, 0.95, textstr, transform=ax.transAxes, fontsize=14,
        verticalalignment='top', bbox=props)
ax.plot(x_interval_for_fit, gauss, label='fit', color="orange")
ax.legend()
ax.set_xlabel("Energy")
ax.set_ylabel("Events")
plt.savefig(f"Fits/plot_{act_energy}GeV_recoe_fit",facecolor='w')


# %%
'''MY CRYSTAL BALL FUNCTION'''
#[1, 1, 4.81, .05, max(reco_e[0])])#alpha, n, mean, sigma, amp
def crystalball(x, alpha, n, mean, sigma, N):#N=amp
    #alpha=1
    n=1.999
    #mean=4.81
    N=max(reco_e[0])
    func = np.array([])
    #print(n)
    #print(x)
    if(math.isnan(n)):
        n=0
    for x in x:
        A = np.float_power(n/abs(alpha),n)*np.exp(-1*(alpha**2)/2)
        B = (n/abs(alpha))-abs(alpha)
        C = (n/abs(alpha))*(np.exp(-1*(alpha**2)/2))/(n-1)
        D = np.sqrt(np.pi/2)*(1+erf(abs(alpha)/np.sqrt(2)))
        #N=1/(sigma*(C+D))
        if((x-mean)/sigma > -1*alpha):
            func = np.append(func, [N*np.exp(-1*((x-mean)**2)/(2*(sigma**2)))])
        elif((x-mean)/sigma <= -1*alpha):
            func = np.append(func, [N*A*(B-(np.float_power((x-mean)/sigma,(-1*n))))])
        print(np.float_power((x-mean)/sigma,(-1*n)))
    #if(func.size==0):
    print(func)
    return func

### --- https://stackoverflow.com/questions/2566412/find-nearest-value-in-numpy-array
def find_nearest(array,value):
    idx,val = min(enumerate(array), key=lambda x: abs(x[1]-value))
    return idx

act_energy = 5 #GeV
ana_file = uproot4.open(f"../work/calib_gam_{act_energy}GeV_100000evt.ana.root")
reco_e = ana_file['ce_emcal_calib;1/reco_energy;1'].to_numpy()
reco_chi2 = ana_file['ce_emcal_calib;1/reco_chi2;1'].to_numpy()

### reduce x range to where energy is
bin_centers = reco_e[1][:-1] + np.diff(reco_e[1]) / 2
energy = np.argmax(reco_e[0])
e_reco = reco_e[1][energy]
e_min = e_reco-.3
e_max = e_reco+.3
arg_min = find_nearest(reco_e[1], e_min)#; arg_min = arg_min[0][0]
arg_max = find_nearest(reco_e[1], e_max)#; arg_max = arg_max[0][0]

fig, ax = plt.subplots()
### plot histogram
width = .85*(reco_e[1][1] - reco_e[1][0])
plt.bar(reco_e[1][arg_min:arg_max], reco_e[0][arg_min:arg_max], align='edge', width=width)

### plot fit
beta, m, scale = act_energy, 3, .4
x_interval_for_fit = np.linspace(reco_e[1][arg_min], reco_e[1][arg_max], 10000)
popt, _ = curve_fit(crystalball, bin_centers[arg_min:arg_max], reco_e[0][arg_min:arg_max], p0=[1, 1, 4.81, .05, max(reco_e[0])])#alpha, n, mean, sigma, amp
print(popt)
fit = crystalball(x_interval_for_fit, *popt)#, 1, 1, 4.81, .05, max(reco_e[0]))#alpha, n, mean, sigma, amp
ax.plot(x_interval_for_fit, fit, label='fit', color="orange")

# place a text box in upper left in axes coords
#textstr = '\n'.join((r'$\mu=%.2f$' % (round(mean,2), ), r'$\sigma=%.2f$' % (round(std, 2), )))
props = dict(boxstyle='round', facecolor='wheat', alpha=0.5)
#ax.text(0.05, 0.95, textstr, transform=ax.transAxes, fontsize=14, verticalalignment='top', bbox=props)

ax.legend()
ax.set_xlabel("Energy")
ax.set_ylabel("Events")


# %%
