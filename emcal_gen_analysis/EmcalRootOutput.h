#ifndef EMCAL_ROOT_OUTPUT_HEADER
#define EMCAL_ROOT_OUTPUT_HEADER

#include <TFile.h>
#include <TDirectory.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TCanvas.h>
#include <TTree.h>
#include <TApplication.h>
#include <TLorentzVector.h>

// This structure is used to fill tree with each electron data to build plots later
struct ElectronRecord {
    uint64_t event_index;
    TLorentzVector p;
    bool is_recoil;
    bool is_in_elcap;
    bool is_in_barrel;
    bool is_in_ioncap;
    double x_true;
    double x_em;
    double y_true;
    double y_em;
    double q2_true;
    double q2_em;
};


class EmcalRootOutput
{
public:
    void init(TFile *file)
    {
        double x_max = 0.3;
        double q2_max = 10;

        // 'locker' locks mutex so other threads can't interfere with TFile doing its job
        std::lock_guard<std::recursive_mutex> locker(lock);

        // create a subdirectory "hist_dir" in this file
        plugin_root_dir = file->mkdir("gen_emcal");
        file->cd();         // Just in case some other root file is the main TDirectory now
        plugin_root_dir->cd();

        // TTree with recoiled electron
//        tree_rec_e = new TTree("el_tree", "a Tree with vect");
//        tree_rec_e->SetDirectory(plugin_root_dir);
//
//        tree_rec_e->Branch("evt_id", &el_record().event_index, "evt_id/i");
//        tree_rec_e->Branch("p", &el_record().p);
//        tree_rec_e->Branch("is_recoil", &el_record().is_recoil);
//        tree_rec_e->Branch("is_in_barrel", &el_record().is_in_barrel);
//        tree_rec_e->Branch("is_in_elcap", &el_record().is_in_elcap);
//        tree_rec_e->Branch("is_in_ioncap", &el_record().is_in_ioncap);
//        tree_rec_e->Branch("x_true", &el_record().x_true);
//        tree_rec_e->Branch("x_em", &el_record().x_em);
//        tree_rec_e->Branch("y_true", &el_record().y_true);
//        tree_rec_e->Branch("y_em", &el_record().y_em);
//        tree_rec_e->Branch("q2_true", &el_record().q2_true);
//        tree_rec_e->Branch("q2_em", &el_record().q2_em);

        //=============================================================================================
        //------------------- electrons -------------------------------

        h1_all_el_e_tot = new TH1D("all_el_e_tot", "Energy of all electrons from primary vtx", 500, 0., 30.);
        h1_all_el_e_tot->GetXaxis()->SetTitle("E [GeV]");

        h1_all_el_theta = new TH1D("all_el_theta", " Theta of all electrons from primary vtx", 100, 0., 180.);
        h1_all_el_theta->GetXaxis()->SetTitle("Theta [deg]");

        h1_el_e_tot = new TH1D("rec_el_e_tot", "Energy of the recoil electron", 500, 0., 30.);
        h1_el_e_tot->GetXaxis()->SetTitle("E [GeV]");

        h1_el_theta = new TH1D("rec_el_theta", " Theta of the recoil electron", 100, 0., 180.);
        h1_el_theta->GetXaxis()->SetTitle("Theta [deg]");

        h1_empz = new TH1F("el_empz" , " E- Pz electron " , 100, 0., 50.   );

        h1_all_el_num = new TH1I("all_el_num", "Number of electrons in an event", 21, 0, 20);
        h1_all_el_num->GetXaxis()->SetTitle("Number of electrons");

        h1_emcal_el_num = new TH1I("emcal_el_num", "Number of electrons that hit any EMCAL", 21, 0, 20);
        h1_emcal_el_num->GetXaxis()->SetTitle("Number of electrons");

        h1_elcap_el_num = new TH1I("elcap_el_num", "Number of electrons in electron cap EMCAL", 21, 0, 20);
        h1_elcap_el_num->GetXaxis()->SetTitle("Number of electrons");

        //=====================================================================
        //------------------- Kinematics X,Q2 -------------------------------

        // Bins and edges for logarithmic x and q2 particles histograms
        const int x_bins=19;
        double x_edges[x_bins + 1] =
                {-3.8, -3.6, -3.4, -3.2, -3.,
                 -2.8, -2.6, -2.4, -2.2, -2.,
                 -1.8, -1.6, -1.4, -1.2, -1.,
                 -0.8, -0.6, -0.4, -0.2, 0.};

        const int q2_bins=16;
        double q2_edges[q2_bins + 1] =
                {0.,   0.25,  0.5,  0.75, 1.,
                 1.25, 1.5,   1.75, 2.,   2.25,
                 2.5,  2.75,  3.,   3.25, 3.5,
                 3.75, 4.};

        // True values
        h2_xq2_true = new TH2I("XQ2_true", " True values Q^{2} vs X ", 100, 0., x_max, 100, 0, q2_max);
        h2_xq2_true->GetXaxis()->SetTitle("x_{true}");
        h2_xq2_true->GetYaxis()->SetTitle("Q^{2}_{true}");

        h2_xq2_true_log = new TH2I("XQ2_true_log", "True values X,Q^{2} log ", x_bins, x_edges, q2_bins, q2_edges);
        h2_xq2_true_log->GetXaxis()->SetTitle("log10(x_{em})");
        h2_xq2_true_log->GetYaxis()->SetTitle("log10(Q^{2}_{em})");

        // Electron method
        h2_xq2_em = new TH2I("XQ2_em", " EM X,Q2 ", 100, 0., x_max, 100, 0, q2_max);
        h2_xq2_em->GetXaxis()->SetTitle("x_{em}");
        h2_xq2_em->GetYaxis()->SetTitle("Q^{2}_{em}");

        h2_xq2_em_log = new TH2I("XQ2_em_log", "EM X,Q2 log ", x_bins, x_edges, q2_bins, q2_edges);
        h2_xq2_em_log->GetXaxis()->SetTitle("log10(x_{em})");
        h2_xq2_em_log->GetYaxis()->SetTitle("log10(Q^{2}_{em})");

        h2_q2em_vs_true = new TH2D("q2em_vs_true", "Q2^{2}_{em} vs Q2^{2}_{true}", 100, 0., q2_max, 100, 0., q2_max);
        h2_q2em_vs_true->GetXaxis()->SetTitle("Q^{2}_{true}");
        h2_q2em_vs_true->GetYaxis()->SetTitle("Q^{2}_{true}-Q2_{em}");

        h2_q2em_vs_true_log = new TH2D("q2em_vs_true_log", "log10(Q2^{2}_{em}) vs log10(Q^{2}_{true})", q2_bins, q2_edges, q2_bins, q2_edges);
        h2_q2em_vs_true_log->GetXaxis()->SetTitle("log10(Q^{2}_{true})");
        h2_q2em_vs_true_log->GetYaxis()->SetTitle("log10(Q^{2}_{em})");

        h2_q2diff_vs_true = new TH2D("q2diff_vs_true", "(Q^{2}_{true}-Q^{2}_{em})/Q^{2}_{true} vs Q^{2}_{true}", 100, 0., q2_max, 100, -0.1, 0.1);
        h2_q2diff_vs_true->GetXaxis()->SetTitle("Q^{2}_{true}");
        h2_q2diff_vs_true->GetYaxis()->SetTitle("(Q^{2}_{true}-Q^{2}_{em})/Q^{2}_{true}");

        h2_q2diff_vs_true_log = new TH2D("q2diff_vs_true_log", "(Q^{2}_{true}-Q^{2}_{em})/Q^{2}_{true} vs Q^{2}_{true}", 100, -1, 2, 100, -1., 1);
        h2_q2diff_vs_true_log->GetXaxis()->SetTitle("log10(Q^{2}_{true})");
        h2_q2diff_vs_true_log->GetYaxis()->SetTitle("(Q^{2}_{true}-Q^{2}_{em})/Q^{2}_{true}");

        h1_dis_x_em = new TH1D("X_em", "X from e-method", 100, 0., x_max);
        h1_dis_x_em->GetXaxis()->SetTitle("X_{em}");

        h1_dis_x_em_log = new TH1D("X_em_log", "X from e-method (log)", x_bins, x_edges);
        h1_dis_x_em->GetXaxis()->SetTitle("X_{em}");

        h1_dis_y_em = new TH1D("Y_true", "True Y ", 100, 0., 1.);
        h1_dis_y_em->GetXaxis()->SetTitle("y_{true}");

        // Hits
        h2_xy_hits_barrel = new TH2I("xy_hits_barrel", "X,Y hits in Ion Endcap EMCAL", 200, -2000, 2000, 200, -2000, 2000);
        h2_xy_hits_elcap = new TH2I("xy_hits_elcap", "X,Y hits in Electron Endcap EMCAL", 200, -1500, 1500, 200, -1500, 1500);
        h2_xy_hits_ioncap = new TH2I("xy_hits_ioncap", "X,Y hits in Central Barrel EMCAL", 200, -2000, 2000, 200, -2000, 2000);
        h1_z_hits_barrel = new TH1I("z_hits_barrel", "Z hits in Central Barrel EMCAL", 100, -3000, 3000);
        h1_z_hits_any = new TH1I("z_hits_any", "Z hits in any EMCAL", 200, -6000, 6000);
    }

    /// This function is a trick to initialize static in header only class
    ElectronRecord& el_record() {
        static ElectronRecord el_record;    // This is used to build a tree
        return el_record;
    }

    // ---- hits ----
    TH2I *h2_xy_hits_barrel;    // X,Y plots for Barrel EMCAL
    TH2I *h2_xy_hits_elcap;     // X,Y plots for Electron endcap EMCAL
    TH2I *h2_xy_hits_ioncap;    // X,Y plots for ION endcap EMCAL
    TH1I *h1_z_hits_barrel;     // Z plots for Barrel EMCAL
    TH1I *h1_z_hits_any;        // Z plots for all EMCALs

    // ---- kinematic variables---
    TH2I *h2_xq2_true;
    TH2I *h2_xq2_true_log;
    TH2I *h2_xq2_em;
    TH2I *h2_xq2_em_log;
    TH1D *h1_dis_x_em;
    TH1D *h1_dis_x_em_log;
    TH1D *h1_dis_y_em;

    TH2D *h2_q2em_vs_true;
    TH2D *h2_q2em_vs_true_log;
    TH2D *h2_q2diff_vs_true;
    TH2D *h2_q2diff_vs_true_log;

    TH1F *h1_empz;

    // ---- Electron ----
    TH1D *h1_all_el_theta;
    TH1D *h1_all_el_e_tot;
    TH1D *h1_el_e_tot;
    TH1D *h1_el_theta;
    TH1I *h1_all_el_num;
    TH1I *h1_emcal_el_num;
    TH1I *h1_elcap_el_num;

    std::recursive_mutex lock;

    TTree * tree_rec_e;     // Tree to store electron related data

private:

    TDirectory* plugin_root_dir;   // Main TDirectory for Plugin histograms and data
};

#endif // OPEN_CHARM_ROOT_OUTPUT_HEADER