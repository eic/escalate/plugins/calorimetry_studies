#include <JANA/JFactoryGenerator.h>

#include "EmcalProcessor.h"

extern "C"
{
    void InitPlugin(JApplication *app)
    {
        InitJANAPlugin(app);

        app->Add(new EmcalProcessor(app));
    }
}
