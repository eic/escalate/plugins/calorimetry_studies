[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/eic%2Fescalate%2Fplugins%2Fcalorimetry_studies/HEAD)

# Calorimetry analysis studies

Contains analysis plugins and processing scripts for study of Electron Cap EMCAL for EIC

Contents:

- ```emcal_analysis/``` - ejana plugin which processes g4e output
- ```notebooks``` - jupyter notebooks with analysis results

### What is in this repo

Directories: 

- emcal_gen_analysis - JANA2 plugin that analyse data without reconstruction (acceptance analysis, combinatorics)
- emcal_reco_analysis - JANA2 plugin that uses reconstruction data
- notebooks - Jupyter notebooks working with data
- profiles - generated EM showers profiles for EMCAL reconstruction
- root_macros - Analysis macros from Mariangela

Files in root 

- G4E simulation (batch mode):
   - g4e_simulation.py - example of g4e simulation in batch mode using BeAGLE file
   - pipeline_simulation.py - almost the same but with changing the geometry in the beginning. 
     the idea is to make it file usable for AI pipeline
   - g4e_pgun.py - Example of particle gun pointing to EMCAL in batch mode
- G4E visualization examples: 
   - g4e_pgun_vis.py - Same particle gun but with Event display. Whole detector is displayed
   - g4e_changed_geom_vis.py - example of changing PWO crystal geometry with visualization
   - g4e_emcal_vis.py - EMCAL centric visualization (most of other elements are off)
- JANA: 
   - ejana_gen_analysis.py  - run gen_analysis plugin
   - ejana_reco_analysis.py - run reco_analysis plugin
- Other notable files: 
   - count_beagle_events_in_file.py - just a handy utility to exactly show BeAGLE file events
   - notebooks/event_display.ipynb - 2D event display of hits and clusters 
   - requirements.txt for python pip to run notebooks, g4e and ejana python wrappers

### Data files

To start with the project, one can use one of the beagle files located here:

[link to a folder on a google drive](https://drive.google.com/drive/folders/1jHLO-w8YZTKUFUIsYmbBjqk58MM41hah?usp=sharing)

Files are large but there is one small sample file with ~30k events (29119)

[link to eC_10x41_GCF_QE_Egaus_small.txt on a google drive](https://drive.google.com/file/d/1C7Mj7rm-Zv21wRTrwzIOhaEHKXZuF7si/view?usp=sharing)


All initial scripts assume that files are downloaded and located in work directory
(directory named **work** is excluded from git to be used as temporary storage)

```
mkdir work
```

The same files can be found at IFarm at:

```
/u/group/eic/mc/BEAGLE/GCF_SRC/g4e_inputfiles
```


### Run in python or jupyter

Edit python files to set the correct input/output file names and a number of events
Run:

```bash
python3 g4e_simulation.py   # to run the simulation
python3 ejana_analysis.py   # to build the plugin and run the analysis
```


### Run in command line

``` bash
ejana
-Pplugins=g4e_reader,emcal_analysis,dis   # read g4e, do ecmcal and dis analysis
-Pemcal_analysis:verbose=1                # verbosity level
-Pemcal_analysis:recoil_only=1            # use only recoil electron for plots
-Pnthreads=1                              # run in single thread (for now)
-Pnevents=1000                            # number of events to generate
-Poutput=/home/romanov/eic/calorimetry_studies/work/beagle2.root
-Pjana:debug_plugin_loading=1
/home/romanov/eic/g4e/g4e-dev/work/beagle2.root
```

One has to run it where emcal_analysis.so is located or add the directory of emcal_analysis.so to JANA_PLUGIN_PATH 
environment variable like this:

```bash
export JANA_PLUGIN_PATH=path_to_plugin_so:$JANA_PLUGIN_PATH   # bash
```

(python does this automatically)



### Running with Pythia8 and HepMC

g4e supports the use of HepMC2 file types in addition to BeAGLE files.

Generate a HepMC file:
```bash
pythia8-run --input <input_filename.cmnd> --hepmc_output <opuput_filename.hepmc> --nevents <nevents>
```
An example .cmnd file is located in the master folder.

Pythia8 outputs a HepMC3 file type which can be converted to HepMC2 using the command:
```bash
HepMC3-convert <hepmc3_input_filename> <hepmc2_output_filename> --input-format hepmc3 --output-format hepmc2
```


### Parameters


- verbose - Plugin output level. 0-almost nothing, 1-some, 2-everything
- only_recoil - Cut that leaves only truly recoil electrons
- e_beam_energy - Energy of colliding electron beam in GeV
- ion_beam_energy - Energy of colliding ion beam in GeV


In command line one sets a parameter like 

```
-P<plugin name>:<param name>=value
``` 

In python/Jupyter one sets a parameter in plugin function

```
jana.plugin(..., param_name=value, ...)
```

### Running with docker

To update / download an image do:

```
docker pull electronioncollider/escalate:dev
```

To run in jupyter notebooks mode:

```bash
docker run -it --rm --name escalate -p8888:8888 electronioncollider/escalate:dev
```

You can bind any directory on your system to docker image by using **-v** flag:
(!) You have to put full path there (!)

```
-v <your/directory>:<docker/directory>
```

Where to share? Docker starts in /home/eicuser/workspace/ where all examples are located. 
If you would like to have all the example, bind your directory to 

```
/home/eicuser/workspace/share
docker run -it --rm -p8888:8888 --name escalate -v /home/romanov/eic/calorimetry_studies:/home/eicuser/workspace/share electronioncollider/escalate:dev
```

You may want to replace /home/eicuser/workspace with examples by this repository. This is convenient
as docker starts in /home/eicuser/workspace and all commands will be thus executed in calorimetry_studies


```
/home/eicuser/workspace/share
docker run -it --rm -p8888:8888 --name escalate -v /home/romanov/eic/calorimetry_studies:/home/eicuser/workspace electronioncollider/escalate:dev
```

When docker is started, you can attach to it and run commands in it like this (you also has notebooks running in your browser) 

```bash
docker exec -it escalate whereis python
```


(!) ALL FURTHER EXAMPLES assume that you bind this repository to /home/eicuser/workspace

To generate pythia8 DIS sample code

```bash
mkdir work  # this directory is omitted from git version control
docker exec -it escalate dire --input dis_pythia8.cmnd --hepmc_output work/py8_dire_dis.hepmc --nevents 10000
```

### Notebooks

```
pip install -r requirments.txt
```

To make plots interactive

```
pip install ipympl
```

Some notebooks use plotly library. Jupyter should just work (ipywidgets must be installed). For jupyterLAB one has to install extensions

https://github.com/plotly/plotly.py#jupyterlab-support-python-35

```

# Basic JupyterLab renderer support
jupyter labextension install jupyterlab-plotly@4.8.2

# OPTIONAL: Jupyter widgets extension for FigureWidget support
jupyter labextension install @jupyter-widgets/jupyterlab-manager plotlywidget@4.8.2

```

# Additional materials

- [PDG Particle Data Group](https://pdg.lbl.gov/)
- [Particle Numbering scheme for MC](https://pdg.lbl.gov/2019/reviews/rpp2019-rev-monte-carlo-numbering.pdf)
- [Particle table](https://pdg.lbl.gov/2007/reviews/montecarlorpp.pdf)
- [pythia8](http://home.thep.lu.se/Pythia/)




