# Make sure pyjano is installed in your system
# do:
#     pip install --upgrade pyjano          # for conda, venv or root install
#     pip install --user --upgrade pyjano   # for user local install
#
#
import pyjano
from pyjano.jana import Jana, PluginFromSource

print(pyjano.__file__)

jana = Jana(nevents=10000, output='work/emcal_gen_analysis.root')
jana.plugin('g4e_reader')
jana.plugin('dis')              # DIS plots

# Ce EMCAL island reconstruction
jana.plugin('islreco', profile='profiles/prof_pwo.dat')


emcal_gen_analysis = PluginFromSource('emcal_gen_analysis')   # Name will be determined from folder name
# add name=<...> for custom name

emcal_gen_analysis.builder.config['cxx_standard'] = 17
# Parameters:
#     verbose      - Plugin output level. 0-almost nothing, 1-some, 2-everything
#     only_recoil  - Cut that leaves only truly recoil electrons
# Beams energies. Defaults are 10x100 GeV
#     e_beam_energy    -  Energy of colliding electron beam");
#     ion_beam_energy  -  Energy of colliding ion beam");
jana.plugin(emcal_gen_analysis, verbose=1)

jana.source('/home/romanov/eic/data/eC_10x41_10k-evt.root')
jana.run()
