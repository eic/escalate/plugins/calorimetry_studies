from g4epy import Geant4Eic
import sys
import pyjano
from pyjano.jana import Jana, PluginFromSource
from cal_auto import *


energies = [3,5,7,9,11]   # range(1, 13)     # [10]  # energies in GeV
particle = 'gamma'  # Common names: proton, e- e+ pi+ pi-, gamma
num_particles = 1
nevents = 10000
# Y shifts [cm]
# 126 - Edge of Glass region
# 100 - Middle of Glass region
# 80  - Transition area (crystal part)
# 40  - Middle of crystals
# 20  - Crystals near the central hole
y = 40    # 100cm shift Middle of Glass region


def g4e_simulate_for_energy(file_name, energy=3, y_shift=40, partice='gamma', num_particles=1, nevents=10000):

    g4e = Geant4Eic()
    g4e.command('/generator/select coneParticleGun')                    # Select cone particle gun generator
    g4e.command(f'/generator/coneParticleGun/particle {particle}')       # Type of particle
    g4e.command('/generator/coneParticleGun/direction 0 0 -1')          # Momentum direction (needs not to be a unit vector)
    g4e.command(f'/generator/coneParticleGun/energy {energy} GeV')       # Set kinetic energy [GeV]
    g4e.command('/generator/coneParticleGun/energyStdDev 0 GeV')        # switch off error
    g4e.command('/generator/coneParticleGun/coneAngleStdDev 0.4 rad')   # Basically is the resulting cone angle
    g4e.command(f'/generator/coneParticleGun/number {num_particles}')    # Set number of particles to be generated

    # g4e.command('/generator/coneParticleGun/position 0 126 -218')     # Edge of Glass region
    # g4e.command(f'/generator/coneParticleGun/position 0 100 -218')    # Middle of Glass region
    # g4e.command('/generator/coneParticleGun/position 0 80 -218')      # Transition area (crystal part)
    # g4e.command('/generator/coneParticleGun/position 0 40 -218')      # Middle of crystals
    # g4e.command('/generator/coneParticleGun/position 0 20 -218')      # Crystals near the central hole

    g4e.command(f'/generator/coneParticleGun/position 0 {y_shift} -218')    # Particle gun point

    g4e.command(['/rootOutput/saveSecondaryLevel 0'])                   # We set it to 0 to have only particle from generator
    g4e.output(file_name)
    g4e.beam_on(nevents).run()


def ejana_process(nevents, input_file_name, output_file_name):

    jana = Jana(nevents=nevents, output=output_file_name)
    jana.plugin('g4e_reader')
    jana.plugin('islreco', inner_profile='profiles/prof_pwo.dat', outer_profile='profiles/prof_lg.dat', verbose=1)

    # Add our analysis plugin (will be auto build from a directory)
    calib_plugin = PluginFromSource('emcal_calibration')   # Name will be determined from folder name
    calib_plugin.builder.config['cxx_standard'] = 17

    # Parameters:
    #     verbose      - Plugin output level. 0-almost nothing, 1-some, 2-everything, 3 - even hits
    jana.plugin(calib_plugin, verbose=1)

    jana.source(input_file_name)
    jana.run()


def main():

    g4e_file_names = [f"work/calib_gam_y{y}cm_{e}GeV_{nevents}evt" for e in energies]
    ejana_input_file_names = [name + ".root" for name in g4e_file_names]
    ejana_output_file_names = [f"work/calib_gam_y{y}cm_{e}GeV_{nevents}evt.ana.root" for e in energies]

    # Simulation loop
    for energy, file_name in zip(energies, g4e_file_names):
        pass#g4e_simulate_for_energy(file_name, energy, y, particle, num_particles, nevents)
        #pass#
    #ejana_input_file_names = [f'work/eD_5x50_1000k-evt.root']
    # Reconstruction loop
    for ejana_input, ejana_output in zip(ejana_input_file_names, ejana_output_file_names):
        pass#ejana_process(nevents, ejana_input, ejana_output)


    mean_list, std_list = reco_fits_single(ejana_output_file_names, energies)
    energy_plots(energies, mean_list, std_list)
    #chi2_plots(ejana_output_file_names, energies)

    #event_display_gun(ejana_input_file_names, ejana_output_file_names, "full", energies)

if __name__ == "__main__":
    main()
