from g4epy import Geant4Eic

g4e = Geant4Eic()

# Values that control ce_emcal geometry ALL IN MM
# 'pwoThickness',      # [mm] Thickness (z direction dimension) of PWO crystals
# 'pwoWidth',          # [mm] Width (and height) of each PWO crystal
# 'pwoGap',            # [mm] Gap between PWO crystals
# 'pwoInnerR',         # [mm] Inner radius or beam hole for PWO block
# 'pwoOuterR',         # [mm] Outer radius of PWO block
# 'glassThickness',    # [mm] Thickness (z direction dimension) of Glass modules
# 'glassWidth',        # [mm] Width (and higth) of each Glass modules
# 'glassGap',          # [mm] Gap between Glass modules

# Changing pwoWidth and gap
# That is too much but the change is easy to spot
new_width = 100
new_gap = 10
g4e.command([f'/g4e/ce_EMCAL/pwoWidth {new_width}',
             f'/g4e/ce_EMCAL/pwoGap {new_gap}'])


# What is the source
# Setup particle gun
# Please see particle gun example for the full explanation
g4e.command('/generator/select coneParticleGun')                    # Select cone particle gun generator
g4e.command('/generator/coneParticleGun/particle e-')               # Fire electrons
g4e.command('/generator/coneParticleGun/direction 0 0 -1')          # Direction to EMCAL
g4e.command('/generator/coneParticleGun/energy 6 GeV')              # Set kinetic energy [GeV]
g4e.command('/generator/coneParticleGun/energyStdDev 1 GeV')        # Energy spread [GeV],
g4e.command('/generator/coneParticleGun/coneAngleStdDev 0.4 rad')   # Cone angle
g4e.command('/generator/coneParticleGun/number 10')                 # N particles to be generated per event

# What is the output
# g4e creates a bunch of files with this name and different extensions
g4e.output('work/change_geo')

# Setup visibility for components
# See emcal_vis example for full explanation
g4e.command([
    '/vis/geometry/set/visibility World_Logic              -1  0',  # Turn off everything in the world
    '/vis/geometry/set/visibility cb_Solenoid_GVol_Logic    0  1',  # Switch on only some components
    '/vis/geometry/set/visibility cb_VTX_GVol_Logic        -1  1',
    '/vis/geometry/set/visibility ce_GEM_GVol_Logic        -1  1',
    '/vis/geometry/set/visibility ce_MRICH_GVol_Logic      -1  1',
    '/vis/geometry/set/visibility ce_EMCAL_GVol_Logic      -1  1',
    '/vis/geometry/set/visibility ce_EMCAL_detPWO_Logic    -1  1',
    '/vis/geometry/set/visibility ce_EMCAL_detGLASS_Logic  -1  1',

    '/vis/geometry/set/colour cb_EMCAL_GVol_Logic        0 ! ! ! 0.1',      # Make not important things transparent
    '/vis/geometry/set/colour cb_HCAL_GVol_Logic         0 ! ! ! 0.1',
    '/vis/geometry/set/colour ci_ENDCAP_GVol_Logic       0 ! ! ! 0.1',
    '/vis/geometry/set/colour ce_GEM_GVol_Logic         -1 ! ! ! 0.1',      # -1 - all sub elements
    '/vis/geometry/set/colour ce_MRICH_GVol_Logic       -1 ! ! ! 0.1',

    '/vis/geometry/set/forceAuxEdgeVisible cb_EMCAL_GVol_Logic  0 1',       # outline barrel emcal
    '/vis/viewer/clearCutawayPlanes',                                       # no clipping (cut away planes
    '/vis/viewer/set/viewpointThetaPhi 90 135',
    '/vis/viewer/zoom 70',
])

# Just in case the default one is:
# g4e.command('/vis/viewer/addCutawayPlane 0  30  0  cm  0. -500 0')
# XY plane
# g4e.command('/vis/viewer/addCutawayPlane  1  0  0  cm   0  0  -0.001')

# Number of events to process
g4e.beam_on(0)

g4e.vis()   # - SHOW EVENT DISPLAY (use less number of events)

# This starts the simulation
g4e.run()

