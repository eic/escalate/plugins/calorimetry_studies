#include <JANA/JApplication.h>


#include <thread>
#include <atomic>
#include <mutex>

#include <JANA/JEventProcessor.h>
#include <JANA/JObject.h>
#include <ejana/EServicePool.h>

#include "Math/Point3D.h"
#include "Math/Vector4D.h"

#include <TLorentzVector.h>
#include <MinimalistModel/McTrack.h>
#include <MinimalistModel/McGeneratedParticle.h>
#include <TH1F.h>
#include <TH2F.h>

#include <ejana/plugins/reco/islreco/CeEmcalCluster.h>
#include <ejana/plugins/io/g4e_reader/GeantHit.h>
#include <ejana/plugins/io/g4e_reader/GeantPrimaryParticle.h>
#include <ejana/plugins/io/g4e_reader/GeantCeEmcalData.h>
#include <TTree.h>

class JApplication;

struct EmcalStatistics {
    uint64_t events_count;
    uint64_t total_el_in_barrel;
    uint64_t total_el_in_elcap;
    uint64_t recoil_el_in_elcap;
    uint64_t recoil_el_in_ioncap;
    uint64_t el_total_count;
};

struct EmcalRootRecord {
    std::vector<uint64_t> calo_hit_id;
    std::vector<double>   calo_hit_idc;
    std::vector<uint64_t> reco_id;
    std::vector<double>   reco_energy;
    std::vector<double>   reco_chi2;
    std::vector<double>   reco_x;
    std::vector<double>   reco_y;
    std::vector<uint64_t> reco_hit_id;
    std::vector<double>   reco_hit_index;
    std::vector<double>   reco_hit_adc;

    void clear() {
        calo_hit_id.clear();
        calo_hit_idc.clear();
        reco_id.clear();
        reco_energy.clear();
        reco_chi2.clear();
        reco_x.clear();
        reco_y.clear();
        reco_hit_id.clear();
        reco_hit_index.clear();
        reco_hit_adc.clear();
    }
};

EmcalRootRecord glb_emcal_evrec;

class EmcalCalibrationProcessor : public JEventProcessor
{
    typedef std::vector<std::pair<const jleic::GeantPrimaryParticle*, const CeEmcalCluster*>> true_reco_pairs_vector;
    typedef std::vector<const jleic::GeantPrimaryParticle*> true_prt_vector;
    typedef std::vector<const CeEmcalCluster*> reco_prt_vector;
public:

    // Constructor just applies
    explicit EmcalCalibrationProcessor(JApplication *app=nullptr):
            JEventProcessor(app),
            services(app)
    {};

    // This is called once before the first call to the Process method
    // below. You may, for example, want to open an output file here.
    // Only one thread will call this.
    void Init() {
        ///  Called once at program start.
        fmt::print("GeneralEmcalAnalysis::Init()\n");

        // Ask service locator for parameter manager. We want to get this plugin parameters.
        // JParameterManager allows to set parameters from command line or python with
        // -Pplugin_name:param_name=value  flags
        auto pm = services.Get<JParameterManager>();

        // Verbosity level. 0 = show nothing. 1 - show some. 2 - mad printer
        // SetDefaultParameter actually sets the parameter value from arguments if it is specified
        m_verbose = 0;
        pm->SetDefaultParameter("emcal_calibration:verbose", m_verbose,
                                "Plugin output level. 0-almost nothing, 1-some, 2-everything");

        // Setup histograms
        auto m_file = services.Get<TFile>();
        m_file->mkdir("ce_emcal_calib")->cd(); // Create a subdir inside dest_file for these results

        double max_energy = 15; //GeV

        h1_true_energy = new TH1F("true_energy", "electron true energy", 500, 0, max_energy);
        h1_reco_energy = new TH1F("reco_energy", "electron reco energy after cuts", 500, 0, max_energy);    // Reconstructed energy
        h1_raw_reco_energy = new TH1F("raw_reco_energy", "electron reco energy ", 500, 0, max_energy);    // Reconstructed energy
        h1_reco_chi2   = new TH1F("reco_chi2", "electron reco chi2", 500, 0, 7);      // Chi2 reconstructed
        h2_cmp_energy  = new TH2F("cmp_energy", "electron energy comparison", 500, 0, max_energy, 100, 0, max_energy);   // Energy comparison
        h2_reco_e_vs_x = new TH2F("reco_e_vs_x", "electron energy comparison", 500, 0, max_energy, 80, 0, 80);
        h2_reco_e_vs_y = new TH2F("reco_e_vs_y", "electron energy comparison", 500, 0, max_energy, 80, 0, 80);
        h2_reco_x_y =  new TH2F("reco_x_y", "electron x_y incidence", 500, 0, 80, 500, 0, 80);
        h2_raw_reco_x_y =  new TH2F("raw_reco_x_y", "electron x_y incidence", 500, 0, 80, 100, 0, 80);

        h1_cluster_size = new TH1F("cluster_size", "electron reco chi2", 8, -0.5, 7.5);
        h1_large_cluster_size = new TH1F("large_cluster_size", "electron reco chi2", 8, -0.5, 7.5);
        h1_true_total_energy = new TH1F("true_total_energy", "electron reco chi2", 100, 0, max_energy);
        h1_emcal_total_yield = new TH1F("emcal_total_yield", "electron reco chi2", 100, 0, max_energy);

        // initialize class that holds histograms and other root objects
        // 'services' is a service locator, we ask it for TFile
        // TTree with recoiled electron
        m_tree = new TTree("emcal_tree", "v1.0");

        m_tree->Branch("calo_hit_id", &glb_emcal_evrec.calo_hit_id);
        m_tree->Branch("calo_hit_idc", &glb_emcal_evrec.calo_hit_idc);
        m_tree->Branch("reco_id", &glb_emcal_evrec.reco_id);
        m_tree->Branch("reco_energy", &glb_emcal_evrec.reco_energy);
        m_tree->Branch("reco_chi2", &glb_emcal_evrec.reco_chi2);
        m_tree->Branch("reco_x", &glb_emcal_evrec.reco_x);
        m_tree->Branch("reco_y", &glb_emcal_evrec.reco_y);
        m_tree->Branch("reco_hit_id", &glb_emcal_evrec.reco_hit_id);
        m_tree->Branch("reco_hit_index", &glb_emcal_evrec.reco_hit_index);
        m_tree->Branch("reco_hit_adc", &glb_emcal_evrec.reco_hit_adc);

        // Zero total statistics
        _stat.events_count = 0;
        _stat.total_el_in_barrel = 0;
        _stat.total_el_in_elcap = 0;
        _stat.recoil_el_in_elcap = 0;
        _stat.recoil_el_in_ioncap = 0;
        _stat.el_total_count = 0;

        // print out the parameters
        if (m_verbose) {
            fmt::print("Parameters:\n");
            fmt::print("  emcal_calibration:verbose         = {0}\n", m_verbose);
        }
    }

    /// This function tries to find a true particle corresponding to each reco particle
    /// (!) At this moment a simplification assumes that there is just a single particle gun true particle per event
    true_reco_pairs_vector GuessTrueToRecoParticles(true_prt_vector true_particles, reco_prt_vector reco_particles)
    {
        true_reco_pairs_vector result;

        // Go through particles
        // Sometimes there are like 2 particles identified, we want one with the largest clusters number
        uint64_t cur_cluster_size=0;
        const CeEmcalCluster* main_reco_particle = nullptr;  // we assume we particle gun just a single particle, this is the particle
        for(auto& particle: reco_particles) {
            if(m_verbose >=2) {
                fmt::print("emcal_calibration: id={:<15} E={:<15} x={:<15} y={:<15} chi2={:<15}\n", particle->id, particle->energy, particle->rel_x, particle->rel_y, particle->chi2);
            }

            // Select particle with the largest cluster size
            if(particle->cluster_size > cur_cluster_size) {
                cur_cluster_size = particle->cluster_size;
                main_reco_particle = particle;
            }
        }

        // (!) Here we assume that we run particle gun that has only 1 particle per event
        const jleic::GeantPrimaryParticle* main_true_particle = nullptr;
        if(true_particles.size()) main_true_particle = true_particles[0];

        if(main_reco_particle && main_true_particle) {
            std::pair<const jleic::GeantPrimaryParticle*, const CeEmcalCluster*> pair(main_true_particle, main_reco_particle);
            result.push_back(pair);
        }
        return result;
    }

    //----------------------------
    // Process
    //
    // This is called for every event. Multiple threads may call this
    // simultaneously. If you write something to an output file here
    // then make sure to protect it with a mutex or similar mechanism.
    // Minimize what is done while locked since that directly affects
    // the multi-threaded performance.
    void Process(const std::shared_ptr<const JEvent>& event)
    {
        ///< Called every event.
        using namespace fmt;
        using namespace std;
        using namespace minimodel;

        // This function is called every event
        if(m_verbose >= 2) fmt::print("Begin of event {} \n", event->GetEventNumber());

        // Clear event record
        glb_emcal_evrec.clear();

        // Get all raw hits
        auto hits = event->Get<jleic::GeantHit>();
        for (auto hit: hits) {

        }

        // Go over true particles
        auto true_particles = event->Get<jleic::GeantPrimaryParticle>();

        double gen_sum_etot = 0;
        // Go through generated particles
        for(auto particle: true_particles) {
            gen_sum_etot += particle->tot_e;
        }
        // Acquire any results you need for your analysis
        auto reco_particles = event->Get<CeEmcalCluster>();

        for(auto reco_particle: reco_particles) {
            auto cluster_size = reco_particle->cluster_size;
            h1_cluster_size->Fill(cluster_size);
            if(cluster_size>2) h1_large_cluster_size->Fill(cluster_size);
        }

        if(reco_particles.size()) h1_true_total_energy->Fill(gen_sum_etot);

        // Acquire any results you need for your analysis
        auto emcal_hits = event->Get<jleic::GeantCeEmcalData>();
        int hits_count = emcal_hits.size();

        double hit_sum_etot = 0;
        // Go through emcal hits
        for(size_t i=0; i< emcal_hits.size(); i++) {
            auto hit = emcal_hits[i];
            hit_sum_etot+=hit->adc;
            fmt::print("{:<15} {}\n", hit_sum_etot, hit->adc);
            glb_emcal_evrec.calo_hit_id.push_back(hit->id);
            glb_emcal_evrec.calo_hit_idc.push_back(hit->adc);
        }
        h1_emcal_total_yield->Fill(hit_sum_etot);

        for(auto cluster: reco_particles) {

        }

        //if(reco_particles.size()) h1_emcal_total_yield->Fill(hit_sum_etot);

        // Set reco particles and true particles
        auto pairs = GuessTrueToRecoParticles(true_particles, reco_particles);

        if(!pairs.size()) return;

        for(auto &pair: pairs) {
            auto true_prt = pair.first;
            auto reco_prt = pair.second;

            h1_true_energy->Fill(true_prt->tot_e);
            h1_raw_reco_energy->Fill(reco_prt->energy);
            h1_reco_chi2->Fill(reco_prt->chi2);
            h2_raw_reco_x_y->Fill(reco_prt->rel_x, reco_prt->rel_y);

            // Applying cuts:
            double x = reco_prt->rel_x - 40;
            double y = reco_prt->rel_y - 40;
            double r = sqrt(x*x + y*y);
            //if(reco_prt->chi2 > 2 || r < 15 || x < -35 || x > 35 || y < -35 || y > 35) continue;            // Throw everything with chi2 > 2
            h1_reco_energy->Fill(reco_prt->energy);
            h2_cmp_energy->Fill(true_prt->tot_e, reco_prt->energy);
            h2_reco_e_vs_x->Fill(reco_prt->energy, reco_prt->rel_x);
            h2_reco_e_vs_y->Fill(reco_prt->energy, reco_prt->rel_y);
            h2_reco_x_y->Fill(reco_prt->rel_x, reco_prt->rel_y);
        }
    }


    //----------------------------
    // Finish
    //
    // This is called once after all events have been processed. You may,
    // for example, want to close an output file here.
    // Only one thread will call this.
    void Finish()
    {
        ///< Called after last event of last event source has been processed.
        fmt::print("EmcalCalibration::Finish(). Statistics:\n");
    };

private:
    ej::EServicePool services;
    EmcalStatistics _stat;


    // Plugin parameters, can be set from the command line
    int m_verbose;                // verbose output level

    TH1F *h1_true_energy;    // True energy
    TH1F *h1_reco_energy;    // Reconstructed energy
    TH1F *h1_reco_chi2;      // Chi2 reconstructed
    TH2F *h2_cmp_energy;     // Energy comparison
    TH1F *h1_raw_reco_energy;   // Reco energy before cuts
    TH1F *h1_cluster_size;
    TH1F *h1_large_cluster_size;
    TH1F *h1_true_total_energy;
    TH1F *h1_emcal_total_yield;
    TH2F *h2_reco_e_vs_x;
    TH2F *h2_reco_e_vs_y;
    TH2F *h2_reco_x_y;
    TH2F *h2_raw_reco_x_y;
    TTree *m_tree;
};


extern "C"
{
    void InitPlugin(JApplication *app)
    {
        InitJANAPlugin(app);
        app->Add(new EmcalCalibrationProcessor(app));
    }
}
