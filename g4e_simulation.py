from g4epy import Geant4Eic

g4e = Geant4Eic()

# Beagle provides beam energy information, but for the sake of simplicity we
g4e.command(['/generator/select beagle',
            '/generator/beagle/verbose 1',
            '/generator/beagle/open work/beagle_q2-1-10_10x100_100k.txt',
            '/run/initialize',
            '/run/beamOn 100000',
            '/rootOutput/saveSecondaryLevel 2'
            ])
g4e.command(['/eic/refdet/eBeam 10',
             '/eic/refdet/pBeam 100'
             ])

# To control how many generation of secondaries (tracks and their hits) to save,
# there is a configuration:
#    /rootOutput/saveSecondaryLevel <ancestry-level> 
#
# <ancestry-level> sets 0-n levels of ancestry which are saved in root file.
#
# Example:
#
# -1 - save everything
# 0 - save only primary particles
# 1 - save primaries and their daughters
# 2 - save primaries, daughters and daughters’ daughters
# n - save n generations of secondaries
#
# (primaries - particles that came from a generator/input file)
#
# The default level is 3, which corresponds to:
#
# /rootOutput/saveSecondaryLevel 3
#
# We set it to 1. If only vertex particles are of the interest, set it to 0
#
# (!) This flag doesn't affect physics in g4e (only what is saved)
# It is controlling resulting files size as they could be huge if everything is written
# EM showers in EMCAL is fully simulated with any value here
#
#g4e.command(['/rootOutput/saveSecondaryLevel 2'])

# What is the source
#g4e.source('work/py8_dire_dis1.hepmc')

# What is the output
# g4e creates a bunch of files with this name and different extensions
g4e.output('work/beagle_100000evt')

#/home/name/Desktop/Physics/Research/JLabs/Simulations/calorimetry_studies/

# Number of events to process
#g4e.beam_on(1000)

# What is the output
# g4e creates a bunch of files with this name and different extensions
g4e.output('work/beagle_100000evt')

#/home/name/Desktop/Physics/Research/JLabs/Simulations/calorimetry_studies/

# Number of events to process
#g4e.beam_on(1000)
>>>>>>> NBranch

# g4e.vis()   # - SHOW EVENT DISPLAY (use less number of events)

# This starts the simulation
g4e.run()

