import uproot4
import matplotlib.pyplot as plt
import matplotlib.patches as patches
from matplotlib.collections import PatchCollection
from matplotlib import cm
from matplotlib.colors import LogNorm, Normalize
import awkward1 as ak

from lxml import etree as ET
import numpy as np
import sys


def get_module_positions(base_name, tree, material='PWO'):

    # <position  name="pos_397" x=... y=...  z=... ... />
    xml_positions = tree.xpath(f"//position[starts-with(@name,'{base_name}_Logic')]")
    positions_by_id = {}
    for xml_position in xml_positions:
        x = float(xml_position.attrib['x'])
        y = float(xml_position.attrib['y'])
        name = xml_position.attrib['name']
        if(material=='PWO'):
            module_id = name[len('ce_EMCAL_detPWO_Logic'+'_'):]
        if(material=='GLASS'):
            module_id = name[len('ce_EMCAL_detGLASS_Logic'+'_'):]
        module_id = int(module_id[:module_id.find('in')])
        positions_by_id[module_id] = (x,y)
    return positions_by_id


def get_module_geometry(base_name, tree):
    update = tree.xpath(f"//box[@name='{base_name}_Solid']")[0]
    unit = update.attrib['lunit']
    size_x = float(update.attrib['x'])
    size_y = float(update.attrib['y'])
    size_z = float(update.attrib['z'])
    return size_x, size_y, size_z, unit

def build_calorimeter_section(ax, positions, size_x, size_y):
    dx = size_x / 2.0
    dy = size_y / 2.0

    module_rects = []
    for position in positions:
        x, y = position
        patch = patches.Rectangle((x-dx, y-dy), width=size_x, height=size_y, edgecolor='black', facecolor='gray')
        module_rects.append(patch)

    col = PatchCollection(module_rects, match_original=True)
    ax.add_collection(col)

    ax.autoscale()
    ax.axis('equal')

    return ax


def plot_calorimeter_hits_full(root_file, ax, pos_by_id, size_x, size_y, start_event, material, process_events=1):

    tree = root_file["events"]

    entry_start=start_event
    entry_stop = start_event + process_events
    events = tree.arrays(['ce_emcal_id', 'ce_emcal_adc'],
                         library="ak", how="zip", entry_start=entry_start, entry_stop=entry_stop)
    #print(events.type)
    #print(events)
    ids = ak.flatten(events.ce_emcal.id)
    weights = ak.flatten(events.ce_emcal.adc)
    #print(ids)
    #print(weights)
    build_calorimeter_section(ax, pos_by_id.values(), size_x, size_y)

    norm = LogNorm()
    norm.autoscale(weights)
    cmap = cm.get_cmap('inferno')

    weights_by_id = {}

    for id, weight in zip(ids, weights):
        if id in weights_by_id.keys():
            weights_by_id[id] += weight
        else:
            weights_by_id[id] = weight

    dx = size_x / 2.0
    dy = size_y / 2.0

    module_rects = []
    for id, weight in weights_by_id.items():
        if(material=='GLASS'):
            if id<1000000:
                continue
        if(material=='PWO'):
            if id>1000000:
                continue
        x,y = pos_by_id[id]
        #print("x: ", x, " y: ", y, " id: ", id, " weight: ", weight)

        patch = patches.Rectangle((x-dx, y-dy), size_x, size_y, edgecolor='black', facecolor=cmap(norm(weight)))
        module_rects.append(patch)

    col = PatchCollection(module_rects, match_original=True)
    ax.add_collection(col)
    return norm, cmap, ax


def plot_calorimeter_hits_sections(root_file, output_file, ax, pos_by_id, size_x, size_y, start_event, material='PWO', section="pwo_pure", process_events=1):
    tree = root_file["events"]

    entry_start=start_event
    entry_stop = start_event + process_events
    events = tree.arrays(['ce_emcal_id', 'ce_emcal_adc'],
                         library="ak", how="zip", entry_start=entry_start, entry_stop=entry_stop)

    ids = ak.flatten(events.ce_emcal.id)
    weights = ak.flatten(events.ce_emcal.adc)
    build_calorimeter_section(ax, pos_by_id.values(), size_x, size_y)


    norm = LogNorm()
    norm.autoscale(weights)
    cmap = cm.get_cmap('inferno')

    for i in range(process_events):
        print(i+start_event)
        entry_start=start_event+i
        entry_stop = entry_start+1
        events = tree.arrays(['ce_emcal_id', 'ce_emcal_adc'],
                         library="ak", how="zip", entry_start=entry_start, entry_stop=entry_stop)
        ids = ak.flatten(events.ce_emcal.id)
        weights = ak.flatten(events.ce_emcal.adc)

        #####################
        weights_by_id = {}

        for id, weight in zip(ids, weights):
            if id in weights_by_id.keys():
                weights_by_id[id] += weight
            else:
                weights_by_id[id] = weight

        dx = size_x / 2.0
        dy = size_y / 2.0
        module_rects = []

        i=0
        max_col = 0
        max_row = 0
        col = 0
        row = 0
        id_list = []
        weights_list = []
        
        for id, weight in weights_by_id.items():
            id_list.append(id)
            weights_list.append(weight)
        maxarg = np.argmax(weights_list)
        id = id_list[maxarg]
        print(weights_by_id[id])
        
        id_str = str(id)
        if len(id_str)==5:
            max_col = int(id_str[:-3])
            max_row = int(id_str[3:])
        if len(id_str)==4:
            max_col = int(id_str[:-3])
            max_row = int(id_str[2:])
        print(max_col)
        print(max_row)

        if(section=='pwo_pure'):
            if(max_col<8 or max_col>67 or max_row<8 or max_row>67): # outside edge
                pass
            elif(max_col<48 and max_col>27 and max_row<48 and max_row>27): # inside edge
                pass
            else:
                for id, weight in weights_by_id.items():
                    if(material=='PWO'):
                        if id>1000000:
                            continue
                        ###### ID for PWO = (column #)0(row #)
                        id_str = str(id)
                        #print(id)
                        if len(id_str)==5:
                            col = int(id_str[:-3])
                            row = int(id_str[3:])
                        if len(id_str)==4:
                            col = int(id_str[:-3])
                            row = int(id_str[2:])
                        #print(col)
                        #print(row)
                    if(material=='GLASS'):
                        if id<1000000:
                            continue
                    
                    x,y = pos_by_id[id]
                    #print("x: ", x, " y: ", y, " id: ", id, " weight: ", weight)

                    i+=1
                    patch = patches.Rectangle((x-dx, y-dy), size_x, size_y, edgecolor='black', facecolor=cmap(norm(weight)))
                    module_rects.append(patch)
                    prev_id = id
                print(i)
                col = PatchCollection(module_rects, match_original=True)
                ax.add_collection(col)
        if(section=='pwo_edge'):
            if(max_col>8 and max_col<67 and max_row>8 and max_row<67): # outside edge
                pass
            else:
                for id, weight in weights_by_id.items():
                    if(material=='PWO'):
                        if id>1000000:
                            continue
                        ###### ID for PWO = (column #)0(row #)
                        id_str = str(id)
                        #print(id)
                        if len(id_str)==5:
                            col = int(id_str[:-3])
                            row = int(id_str[3:])
                        if len(id_str)==4:
                            col = int(id_str[:-3])
                            row = int(id_str[2:])
                        #print(col)
                        #print(row)
                    if(material=='GLASS'):
                        if id<1000000:
                            continue
                    
                    x,y = pos_by_id[id]
                    #print("x: ", x, " y: ", y, " id: ", id, " weight: ", weight)

                    i+=1
                    patch = patches.Rectangle((x-dx, y-dy), size_x, size_y, edgecolor='black', facecolor=cmap(norm(weight)))
                    module_rects.append(patch)
                    prev_id = id
                print(i)
                col = PatchCollection(module_rects, match_original=True)
                ax.add_collection(col)
    return norm, cmap, ax