# This file generates particles close to the surface of calorimeter
import sys
from g4epy import Geant4Eic

# Number of events and energy
nevents = int(sys.argv[1]) if len(sys.argv) > 1 else 5000
energy = float(sys.argv[2]) if len(sys.argv) > 2 else 4  #GeV

g4e = Geant4Eic()

# Select cone particle gun generator
g4e.command('/generator/select coneParticleGun')

# Set particle to be generated.
# (geantino is default)
# (ion can be specified for shooting ions)
# Common names: proton, e- e+ pi+ pi-
g4e.command('/generator/coneParticleGun/particle e-')

# Set momentum direction
# Direction needs not to be a unit vector
g4e.command('/generator/coneParticleGun/direction 0 0 -1')

# Set kinetic energy [GeV]
g4e.command(f'/generator/coneParticleGun/energy {energy} GeV')

# Energy spread [GeV],
# energy is smeared as gauss (mean=<particle E>, stddev=energyStdDev)
g4e.command('/generator/coneParticleGun/energyStdDev 0.1 GeV')

# Cone angle standard deviation.
# Basically is the resulting cone angle
g4e.command('/generator/coneParticleGun/coneAngleStdDev 0.1 rad')

# Set number of particles to be generated
g4e.command('/generator/coneParticleGun/number 1')

# Edge of Glass region
# g4e.command('/generator/coneParticleGun/position 0 126 -218')

# Middle of Glass region
g4e.command('/generator/coneParticleGun/position 0 100 -218')

# Transition area (crystal part)
# g4e.command('/generator/coneParticleGun/position 0 80 -218')

# Middle of crystals
# g4e.command('/generator/coneParticleGun/position 0 40 -218')

# Crystals near the central hole
# g4e.command('/generator/coneParticleGun/position 0 20 -218')

# To control how many generation of secondaries (tracks and their hits) to save,
# there is a configuration:
#    /rootOutput/saveSecondaryLevel <ancestry-level>
#
# <ancestry-level> sets 0-n levels of ancestry which are saved in root file.
#
# Example:
#
# -1 - save everything
# 0 - save only primary particles
# 1 - save primaries and their daughters
# 2 - save primaries, daughters and daughters’ daughters
# n - save n generations of secondaries
#
# (primaries - particles that came from a generator/input file)
#
# The default level is 3, which corresponds to:
#
# /rootOutput/saveSecondaryLevel 3
#
# We set it to 1. If only vertex particles are of the interest, set it to 0
#
# This flag doesn't affect physics in g4e (only what is saved)
# so EM showers in EMCAL is fully simulated with any value here
g4e.command(['/rootOutput/saveSecondaryLevel 1'])

# Extension is omitted here
# g4e creates a bunch of files with this name and different extensions
g4e.output('work/pgun_6GeV')

# Run g4e run!!!
#g4e.beam_on(300).run()

# Extension is omitted here
# g4e creates a bunch of files with this name and different extensions
g4e.output(f'work/close_pgun_gamma_{energy}GeV_{nevents}')
print("Generating: ")
print(f"event count: {nevents}")

# Run
g4e.beam_on(nevents).run()
