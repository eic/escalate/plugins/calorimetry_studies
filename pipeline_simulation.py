from g4epy import Geant4Eic

g4e = Geant4Eic(beamline='erhic')

# Values that control ce_emcal geometry ALL IN MM
# 'pwoThickness',      # [mm] Thickness (z direction dimension) of PWO crystals
# 'pwoWidth',          # [mm] Width (and height) of each PWO crystal
# 'pwoGap',            # [mm] Gap between PWO crystals
# 'pwoInnerR',         # [mm] Inner radius or beam hole for PWO block
# 'pwoOuterR',         # [mm] Outer radius of PWO block
# 'glassThickness',    # [mm] Thickness (z direction dimension) of Glass modules
# 'glassWidth',        # [mm] Width (and higth) of each Glass modules
# 'glassGap',          # [mm] Gap between Glass modules

# Changing pwoWidth and gap
# That is too much but the change is easy to spot
new_width = 100
new_gap = 10
new_pwo_thick   = 200 #mm
new_pwo_width   = 20  #mm
new_pwo_gap     = 0.1 #mm
new_pwo_innR    = 150 #mm  #fixed
new_pwo_outR    = 490 #mm  #fixed
new_glass_thick = 200 #mm
new_glass_width = 20 #mm
new_glass_gap   = 0.1 #mm

g4e.command([f'/g4e/ce_EMCAL/pwoThickness {new_pwo_thick}',
             f'/g4e/ce_EMCAL/pwoWidth {new_pwo_width}',
             f'/g4e/ce_EMCAL/pwoGap {new_pwo_gap}',
             f'/g4e/ce_EMCAL/pwoInnerR {new_pwo_innR}',
             f'/g4e/ce_EMCAL/pwoOuterR {new_pwo_outR}',
             f'/g4e/ce_EMCAL/glassThickness {new_glass_thick}',
             f'/g4e/ce_EMCAL/glassWidth {new_glass_width}',
             f'/g4e/ce_EMCAL/glassGap {new_glass_gap}',
             ])



# Beagle provides beam energy information, but for the sake of simplicity we
g4e.command(['/detsetup/eBeam 10',
             '/detsetup/pBeam 41'
             ])

# To control how many generation of secondaries (tracks and their hits) to save,
# there is a configuration:
#    /rootOutput/saveSecondaryLevel <ancestry-level>
#
# <ancestry-level> sets 0-n levels of ancestry which are saved in root file.
#
# Example:
#
# -1 - save everything
# 0 - save only primary particles
# 1 - save primaries and their daughters
# 2 - save primaries, daughters and daughters’ daughters
# n - save n generations of secondaries
#
# (primaries - particles that came from a generator/input file)
#
# The default level is 3, which corresponds to:
#
# /rootOutput/saveSecondaryLevel 3
#
# We set it to 1. If only vertex particles are of the interest, set it to 0
#
# (!) This flag doesn't affect physics in g4e (only what is saved)
# It is controlling resulting files size as they could be huge if everything is written
# EM showers in EMCAL is fully simulated with any value here
#
g4e.command(['/rootOutput/saveSecondaryLevel 2'])

# What is the source
g4e.source('work/eC_10x41_GCF_QE_Egaus_small.txt')

# What is the output
# g4e creates a bunch of files with this name and different extensions
g4e.output('work/eC_10x41_10k-evt')

# Number of events to process
g4e.beam_on(10000)

# g4e.vis()   # - SHOW EVENT DISPLAY (use less number of events)

# This starts the simulation
g4e.run()

