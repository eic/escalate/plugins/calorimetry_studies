# This file generates particles close to the surface of calorimeter

from g4epy import Geant4Eic

g4e = Geant4Eic()

# Select cone particle gun generator
g4e.command('/generator/select coneParticleGun')

# Set particle to be generated.
# (geantino is default)
# (ion can be specified for shooting ions)
# Common names: proton, e- e+ pi+ pi-
g4e.command('/generator/coneParticleGun/particle gamma')

# Set momentum direction
# Direction needs not to be a unit vector
g4e.command('/generator/coneParticleGun/direction 0 0 -1')

# Set kinetic energy [GeV]
g4e.command('/generator/coneParticleGun/energy 6 GeV')

# Energy spread [GeV],
# energy is smeared as gauss (mean=<particle E>, stddev=energyStdDev)
g4e.command('/generator/coneParticleGun/energyStdDev 1 GeV')

# Cone angle standard deviation.
# Basically is the resulting cone angle
g4e.command('/generator/coneParticleGun/coneAngleStdDev 0.1 rad')

# Set number of particles to be generated
g4e.command('/generator/coneParticleGun/number 1')

# Edge of Glass region
# g4e.command('/generator/coneParticleGun/position 0 126 -218')

# Middle of Glass region
g4e.command('/generator/coneParticleGun/position 0 100 -218')

# Transition area (crystal part)
# g4e.command('/generator/coneParticleGun/position 0 80 -218')

# Middle of crystals
# g4e.command('/generator/coneParticleGun/position 0 40 -218')

# Crystals near the central hole
# g4e.command('/generator/coneParticleGun/position 0 20 -218')


# To control how many generation of secondaries (tracks and their hits) to save,
# there is a configuration:
#    /rootOutput/saveSecondaryLevel <ancestry-level>
#
# <ancestry-level> sets 0-n levels of ancestry which are saved in root file.
#
# Example:
#
# -1 - save everything
# 0 - save only primary particles
# 1 - save primaries and their daughters
# 2 - save primaries, daughters and daughters’ daughters
# n - save n generations of secondaries
#
# (primaries - particles that came from a generator/input file)
#
# The default level is 3, which corresponds to:
#
# /rootOutput/saveSecondaryLevel 3
#
# We set it to 1. If only vertex particles are of the interest, set it to 0
#
# This flag doesn't affect physics in g4e (only what is saved)
# so EM showers in EMCAL is fully simulated with any value here
g4e.command(['/rootOutput/saveSecondaryLevel 1'])

# Extension is omitted here
# g4e creates a bunch of files with this name and different extensions
g4e.output('work/pgun_6GeV')

# Initialize graphics
g4e.vis()


# we will switch things on and off
# /vis/geometry/set/visibility has 3 parameters:
# /vis/geometry/set/visibility <logic name> <depth> <on/off>
# If depth -1 - all children are affected
# Turn off everything in the world
g4e.command('/vis/geometry/set/visibility World_Logic -1 0')

g4e.command([
    '/vis/geometry/set/visibility cb_Solenoid_GVol_Logic  0  1',
    '/vis/geometry/set/visibility cb_EMCAL_GVol_Logic     0  1',
    '/vis/geometry/set/visibility cb_HCAL_GVol_Logic      0  1',
    '/vis/geometry/set/visibility ci_ENDCAP_GVol_Logic    0  1',
    '/vis/geometry/set/visibility cb_VTX_GVol_Logic      -1  1',
    '/vis/geometry/set/visibility ce_GEM_GVol_Logic      -1  1',
    '/vis/geometry/set/visibility ce_MRICH_GVol_Logic    -1  1'
])

# Make them transparent
# ! - means omit parameter (those are RGB)
g4e.command([
    '/vis/geometry/set/colour cb_EMCAL_GVol_Logic        0 ! ! ! 0.1',
    '/vis/geometry/set/colour cb_HCAL_GVol_Logic         0 ! ! ! 0.1',
    '/vis/geometry/set/colour ci_ENDCAP_GVol_Logic       0 ! ! ! 0.1',
    '/vis/geometry/set/colour ce_GEM_GVol_Logic         -1 ! ! ! 0.1',  # -1 - all sub elements
    '/vis/geometry/set/colour ce_MRICH_GVol_Logic       -1 ! ! ! 0.1',
    '/vis/geometry/set/forceAuxEdgeVisible cb_EMCAL_GVol_Logic     0  1',
])


g4e.command([
    '/vis/geometry/set/visibility ce_EMCAL_GVol_Logic      -1 1',
    '/vis/geometry/set/visibility ce_EMCAL_detPWO_Logic    -1 1',
    '/vis/geometry/set/visibility ce_EMCAL_detGLASS_Logic  -1 1',
])

# There are a lot of particles
g4e.command('/vis/ogl/set/displayListLimit 100000')

# We want to clear default cutaway plane:
# g4e.command('/vis/viewer/set/cutawayMode intersection')
g4e.command(['/vis/viewer/clearCutawayPlanes',
             '/vis/viewer/set/viewpointThetaPhi 180 0 deg'
             '/vis/viewer/zoom 0.015'])

# Just in case the default one is:
# g4e.command('/vis/viewer/addCutawayPlane 0  30  0  cm  0. -500 0')
# XY plane
# g4e.command('/vis/viewer/addCutawayPlane  1  0  0  cm   0  0  -0.001')



# Run with event display
g4e.beam_on(1).run()
