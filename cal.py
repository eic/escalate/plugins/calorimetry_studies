import uproot4
#from ROOT import TH1F
from hist import Hist
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.patches as patches
from matplotlib.collections import PatchCollection
from matplotlib import cm
from matplotlib.colors import LogNorm, Normalize
from uncertainties import unumpy as unp
import numpy as np
import awkward1 as ak
from scipy.optimize import curve_fit
import sys
import math
from scipy.stats import crystalball

mpl.rcParams['text.usetex'] = True
plt.rc('text', usetex=False)

''' Functions and variables for next two bins '''
### --- https://stackoverflow.com/questions/2566412/find-nearest-value-in-numpy-array
def find_nearest(array,value):
    idx,val = min(enumerate(array), key=lambda x: abs(x[1]-value))
    return idx

### --- https://stackoverflow.com/questions/35544233/fit-a-curve-to-a-histogram-in-python
def gaussian(x, mean, amplitude, standard_deviation):
    return amplitude * np.exp( - (x - mean)**2 / (2*standard_deviation ** 2))

def reco_fits(ana_files, energies):
    fig, ax = plt.subplots(len(ana_files),figsize=(10, 20))
    fig.suptitle(r'Reco Energy', fontsize=13)
    mean_list = []
    std_list = []
    ### reduce x range to where energy is
    for i, file_path in enumerate(ana_files):
        file_ = uproot4.open(file_path)
        reco_e = file_['ce_emcal_calib;1/reco_energy;1'].to_numpy()
        bin_centers = reco_e[1][:-1] + np.diff(reco_e[1]) / 2
        energy = np.argmax(reco_e[0])
        e_min = reco_e[1][energy]-.3
        e_max = reco_e[1][energy]+.3
        arg_min = find_nearest(reco_e[1], e_min)#; arg_min = arg_min[0][0]
        arg_max = find_nearest(reco_e[1], e_max)#; arg_max = arg_max[0][0]

        x_interval_for_fit = np.linspace(reco_e[1][arg_min], reco_e[1][arg_max], 10000)
        popt, _ = curve_fit(gaussian, reco_e[1][arg_min:arg_max], bin_centers[arg_min:arg_max], p0=[1., 0., energies[i]])
        gauss = gaussian(x_interval_for_fit, *popt)
        gauss_mean = np.mean(gauss)
        gauss_std = np.std(gauss)
        mean_list.append(gauss_mean)
        std_list.append(gauss_std)
        '''print("mean: ", round(gauss_mean,2))
        print("std:  ", round(gauss_std, 2))'''

        ### plot fit
        popt, _ = curve_fit(gaussian, bin_centers[arg_min:arg_max], reco_e[0][arg_min:arg_max], p0=[energies[i],max(reco_e[0]),1.])
        gauss = gaussian(x_interval_for_fit, *popt)
        
        ### plot histogram
        width = .85*(reco_e[1][1] - reco_e[1][0])
        ax[i].bar(reco_e[1][arg_min:arg_max], reco_e[0][arg_min:arg_max], align='edge', width=width)
        textstr = '\n'.join((
            r'$\mu=%.2f$' % (round(gauss_mean,2), ),
            r'$\sigma=%.2f$' % (round(gauss_std, 2), )))
        # these are matplotlib.patch.Patch properties
        props = dict(boxstyle='round', facecolor='wheat', alpha=0.5)
        # place a text box in upper left in axes coords
        ax[i].text(0.05, 0.95, textstr, transform=ax[i].transAxes, fontsize=14,
                verticalalignment='top', bbox=props)
        ax[i].plot(x_interval_for_fit, gauss, label='fit', color="orange")
        ax[i].legend()
        ax[i].set_xlabel("Energy")
        ax[i].set_ylabel("Events")
    return mean_list, std_list
    plt.savefig(f"Fits/plot_recoe_fit",facecolor='w')


'''
real energy vs reconstructed energy
real energy/reconstructed energy vs energy
sigma energy vs energy
(sigma energy/energy) * 100% vs energy
'''
def plots(energies, mean_list, std_list):
    fig, ax = plt.subplots(4,figsize=(10, 30))
    fig.suptitle(r'Plots', fontsize=13)
    
    ### --- Real Energy vs Reconstructed Energy --- ###
    ax[0].set_title("Real Energy vs Reconstructed Energy")
    
    ax[0].scatter(mean_list, energies)
    ax[0].set_xlabel("Reconstructed Energy (GeV)")
    ax[0].set_ylabel("Real Energy (GeV)")
    ax[0].set_xticks(range(math.floor(min(mean_list)), math.ceil(max(mean_list))+1))
    ax[0].set_yticks(range(math.floor(min(energies)), math.ceil(max(energies))+1))


    ### --- Real Energy/Reconstructed Energy vs Energy --- ###
    ratio = []
    for i in range(len(mean_list)):
        #if(energies[i]==i):
        ratio.append(energies[i]/mean_list[i])
        #else:
        #    ratio.append(0)
    ax[1].set_title("Real Energy/Reconstructed Energy vs Energy")
    ax[1].scatter(energies, ratio)
    ax[1].set_xlabel("Energy (GeV)")
    ax[1].set_ylabel("Real Energy/Reconstructed Energy")


    ### --- Sigma Energy vs Energy --- ###
    ax[2].set_title(r'$\sigma$ Energy vs Energy')
    ax[2].scatter(energies, std_list)
    ax[2].set_xlabel("Energy (GeV)")
    ax[2].set_ylabel(r'$\sigma$ Energy (GeV)')
    ax[2].set_xticks(range(math.floor(min(energies)), math.ceil(max(energies))+1))


    ### --- (Sigma Energy/Energy)*100% vs Energy --- ###
    ratio = []
    for i in range(len(std_list)):
        ratio.append((std_list[i]/energies[i])*100)
    ax[3].set_title(r'$\sigma$ Energy/Energy vs Energy')
    ax[3].scatter(energies, ratio)
    ax[3].set_xlabel("Energy (GeV)")
    ax[3].set_ylabel(r'$\sigma$ Energy/Energy $\%$')
    ax[3].set_xticks(range(math.floor(min(energies)), math.ceil(max(energies))+1))

    plt.savefig(f"Fits/other_plots",facecolor='w')