# Make sure pyjano is installed in your system
# do:
#     pip install --upgrade pyjano          # for conda, venv or root install
#     pip install --user --upgrade pyjano   # for user local install
#
#
import pyjano
from pyjano.jana import Jana, PluginFromSource

print(pyjano.__file__)

input_file_name = 'work/close_pgun_gamma_4GeV_5000.root'
output_file_name = input_file_name.replace('.root', '.ana.root')

jana = Jana(nevents=10000, output=output_file_name).source(input_file_name)

jana = Jana(nevents=1000, output='work/test_gen_analysis.ana.root')
jana.plugin('g4e_reader')
    #.plugin('dis')              # DIS plots

# Ce EMCAL island reconstruction
jana.plugin('islreco', profile='profiles/prof_pwo.dat', verbose=3, adc_cut=5)


# Add our analysis plugin (will be auto build from a directory)
calib_plugin = PluginFromSource('emcal_calibration')   # Name will be determined from folder name
calib_plugin.builder.config['cxx_standard'] = 17

# Parameters:
<<<<<<< HEAD
#     verbose      - Plugin output level. 0-almost nothing, 1-some, 2-everything
#     only_recoil  - Cut that leaves only truly recoil electrons
# Beams energies. Defaults are 10x100 GeV
#     e_beam_energy    -  Energy of colliding electron beam");
#     ion_beam_energy  -  Energy of colliding ion beam");
jana.plugin(emcal_gen_analysis, verbose=1)

jana.source('work/try.root')
=======
#     verbose      - Plugin output level. 0-almost nothing, 1-some, 2-everything, 3 - even hits
jana.plugin(calib_plugin, verbose=3)


>>>>>>> NBranch
jana.run()
