import uproot4
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.patches as patches
from matplotlib.collections import PatchCollection
from matplotlib import cm
from matplotlib.colors import LogNorm, Normalize
from uncertainties import unumpy as unp
import numpy as np
import awkward1 as ak
import sys
import math

### --- Curve Fit Imports
from scipy.stats import crystalball
from scipy.optimize import curve_fit
from hist import Hist

### --- Event Display Imports
from event_display import get_module_geometry, get_module_positions, plot_calorimeter_hits_full, plot_calorimeter_hits_sections
from lxml import etree as ET

mpl.rcParams['text.usetex'] = True
plt.rc('text', usetex=False)

''' Functions and variables for next two bins '''
### --- https://stackoverflow.com/questions/2566412/find-nearest-value-in-numpy-array
def find_nearest(array,value):
    idx,val = min(enumerate(array), key=lambda x: abs(x[1]-value))
    return idx

### --- https://stackoverflow.com/questions/35544233/fit-a-curve-to-a-histogram-in-python
def gaussian(x, mean, amplitude, standard_deviation):
    return amplitude * np.exp( - (x - mean)**2 / (2*standard_deviation ** 2))

### --- https://en.wikipedia.org/wiki/Crystal_Ball_function
def crystalball(x, alpha, n, mean, sigma, N):#N=amp
    func = np.array([])
    for x in x:
        A = np.float_power(n/abs(alpha),n)*np.exp(-1*(alpha**2)/2)
        B = (n/abs(alpha))-abs(alpha)
        if((x-mean)/sigma > -1*alpha):
            func = np.append(func, [N*np.exp(-1*((x-mean)**2)/(2*(sigma**2)))])
        elif((x-mean)/sigma <= -1*alpha):
            func = np.append(func, [N*A*((np.float_power((B-(x-mean)/sigma),(-1*n))))])
    return func

def reco_fits(ana_files, energies):
    fig, ax = plt.subplots(len(ana_files)+1,figsize=(10, 20))
    fig.suptitle(r'Reco Energy', fontsize=13)
    mean_list = []
    std_list = []
    
    for i, file_path in enumerate(ana_files):
        file_ = uproot4.open(file_path)
        reco_e = file_['ce_emcal_calib;1/reco_energy;1'].to_numpy()

        ### --- reduce x range around energy
        energy = np.argmax(reco_e[0])
        e_min = reco_e[1][energy]-1
        e_max = reco_e[1][energy]+1
        arg_min = find_nearest(reco_e[1], e_min)
        arg_max = find_nearest(reco_e[1], e_max)

        ### --- plot histogram
        width = .85*(reco_e[1][1] - reco_e[1][0])
        ax[i].bar(reco_e[1][arg_min:arg_max], reco_e[0][arg_min:arg_max], align='edge', width=width)

        ### --- plot fit with Crystal Ball function
        bin_centers = reco_e[1][:-1] + np.diff(reco_e[1]) / 2
        x_interval_for_fit = np.linspace(reco_e[1][arg_min], reco_e[1][arg_max], 10000)
        popt, _ = curve_fit(crystalball, bin_centers[arg_min:arg_max], reco_e[0][arg_min:arg_max], p0=[1, 1, reco_e[1][energy], .05, max(reco_e[0])]) # alpha, n, mean, sigma, amp
        fit = crystalball(x_interval_for_fit, *popt) # alpha, n, mean, sigma, amp
        ax[i].plot(x_interval_for_fit, fit, label='fit', color="orange")
        mean = popt[2]
        mean_list.append(mean)
        std = popt[3]
        std_list.append(std)

        ### --- print mean and std on graph
        textstr = '\n'.join((r'$\mu=%.2f$' % (round(mean,2), ), r'$\sigma=%.2f$' % (round(std, 2), )))
        props = dict(boxstyle='round', facecolor='wheat', alpha=0.5)
        ax[i].text(0.05, 0.95, textstr, transform=ax[i].transAxes, fontsize=14, verticalalignment='top', bbox=props)

        ax[i].legend()
        ax[i].set_xlabel("Energy")
        ax[i].set_ylabel("Events")
    plt.savefig(f"Plots_Fits/3GeV_y40cm_plot_recoe_fit",facecolor='w')
    return mean_list, std_list
    
def reco_fits_single(ana_files, energies):
    mean_list = []
    std_list = []
    
    for i, file_path in enumerate(ana_files):
        fig, ax= plt.subplots(1, 1, figsize=(7, 7))
        file_ = uproot4.open(file_path)
        reco_e = file_['ce_emcal_calib;1/reco_energy;1'].to_numpy()

        ### --- reduce x range around energy
        energy = np.argmax(reco_e[0])
        e_min = reco_e[1][energy]-.5
        e_max = reco_e[1][energy]+.5
        arg_min = find_nearest(reco_e[1], e_min)
        arg_max = find_nearest(reco_e[1], e_max)

        ### --- plot histogram
        width = .85*(reco_e[1][1] - reco_e[1][0])
        ax.bar(reco_e[1][arg_min:arg_max], reco_e[0][arg_min:arg_max], align='edge', width=width)

        ### --- plot fit with Crystal Ball function
        bin_centers = reco_e[1][:-1] + np.diff(reco_e[1]) / 2
        x_interval_for_fit = np.linspace(reco_e[1][arg_min], reco_e[1][arg_max], 10000)
        popt, _ = curve_fit(crystalball, bin_centers[arg_min:arg_max], reco_e[0][arg_min:arg_max], p0=[1, 1, reco_e[1][energy], .05, max(reco_e[0])]) # alpha, n, mean, sigma, amp
        fit = crystalball(x_interval_for_fit, *popt) # alpha, n, mean, sigma, amp
        ax.plot(x_interval_for_fit, fit, label='fit', color="orange")
        mean = popt[2]
        mean_list.append(mean)
        std = popt[3]
        std_list.append(std)

        ### --- print mean and std on graph
        textstr = '\n'.join((r'$\mu=%.4f$' % (round(mean,4), ), r'$\sigma=%.4f$' % (round(std, 4), )))
        props = dict(boxstyle='round', facecolor='wheat', alpha=0.5)
        ax.text(0.05, 0.95, textstr, transform=ax.transAxes, fontsize=14, verticalalignment='top', bbox=props)

        ax.legend()
        title = "{} GeV Histogram".format(energies[i])
        ax.set_title(title, fontsize=25)
        ax.set_xlabel("Energy", fontsize=20)
        ax.set_ylabel("Events", fontsize=20)
        filename = "Plots_Fits/y40cm_{}GeV_plot_recoe_fit".format(energies[i])#_{}GeV".format(energies[i])"
        plt.savefig(filename, facecolor='w')
    return mean_list, std_list


def energy_plots(energies, mean_list, std_list):
    # energies  =   true energy
    # mean_list =   reconstructed energy
    # std_list  =   standard deviation from fits

    fig = plt.figure(figsize=(20, 15))
    
    '''### --- True Energy vs Reconstructed Energy --- ###
    plt.title("True Energy vs Reconstructed Energy", fontsize=45)
    plt.scatter(energies, mean_list, s=100)
    plt.xlabel("Reconstructed Energy (GeV)", fontsize=40)
    plt.ylabel("True Energy (GeV)", fontsize=40)
    plt.xticks(range(math.floor(min(mean_list)), math.ceil(max(mean_list))+1), fontsize=35)
    plt.yticks(range(math.floor(min(energies)), math.ceil(max(energies))+1), fontsize=35)
    plt.savefig(f"Plots_Fits/90realvsreco",facecolor='w')
    plt.clf()'''

    ### --- Reconstructed Energy vs True Energy --- ###
    plt.title("Reconstructed Energy vs True Energy", fontsize=45)
    plt.scatter(mean_list, energies, s=100)
    plt.ylabel("Reconstructed Energy (GeV)", fontsize=40)
    plt.xlabel("True Energy (GeV)", fontsize=40)
    plt.yticks(range(math.floor(min(mean_list)), math.ceil(max(mean_list))+1), fontsize=35)
    plt.xticks(range(math.floor(min(energies)), math.ceil(max(energies))+1), fontsize=35)
    plt.savefig(f"Plots_Fits/y40cm_recovsreal",facecolor='w')
    plt.clf()
    '''
    ### --- True Energy/Reconstructed Energy vs Energy --- ###
    ratio = []
    for i in range(len(mean_list)):
        ratio.append(energies[i]/mean_list[i])
    plt.title("True Energy/Reconstructed Energy vs True Energy", fontsize=45)
    plt.scatter(energies, ratio, s=100)
    plt.xlabel("Energy (GeV)", fontsize=40)
    plt.ylabel("True Energy/Reconstructed Energy", fontsize=40)
    plt.xticks(range(math.floor(min(energies)), math.ceil(max(energies))+1), fontsize=35)
    plt.yticks(fontsize=35)
    plt.savefig(f"Plots_Fits/y40cm_realrecoEvsE",facecolor='w')
    plt.clf()'''

    ### --- Reconstructed Energy/True Energy x 100% vs Energy --- ###
    ratio = []
    for i in range(len(mean_list)):
        ratio.append(mean_list[i]/energies[i]*100)
    plt.title(r'Reconstructed Energy/True Energy $\times$ 100% vs True Energy', fontsize=45)
    plt.scatter(energies, ratio, s=100)
    plt.xlabel("Energy (GeV)", fontsize=40)
    plt.ylabel(r'Reconstructed Energy/True Energy $\times$ 100%', fontsize=36)
    plt.xticks(range(math.floor(min(energies)), math.ceil(max(energies))+1), fontsize=35)
    plt.yticks(fontsize=35)
    plt.savefig(f"Plots_Fits/y40cm_recorealEvsE",facecolor='w')
    plt.clf()

    ### --- Sigma Energy vs Energy --- ###
    plt.title(r'$\sigma$ Energy vs True Energy', fontsize=45)
    plt.scatter(energies, std_list, s=100)
    plt.xlabel("Energy (GeV)", fontsize=40)
    plt.ylabel(r'$\sigma$ Energy (GeV)', fontsize=40)
    plt.xticks(range(math.floor(min(energies)), math.ceil(max(energies))+1), fontsize=35)
    plt.yticks(fontsize=35)
    plt.savefig(f"Plots_Fits/y40cm_sigvsE",facecolor='w')
    plt.clf()

    ### --- (Sigma Energy/Energy)*100% vs Energy --- ###
    ratio = []
    for i in range(len(std_list)):
        ratio.append((std_list[i]/energies[i])*100)
    plt.title(r'$\sigma$ Energy/True Energy $\times$ 100% vs True Energy', fontsize=45)
    plt.scatter(energies, ratio, s=100)
    plt.xlabel("True Energy (GeV)", fontsize=40)
    plt.ylabel(r'$\sigma$ Energy/True Energy $\times$ 100%', fontsize=40)
    plt.xticks(range(math.floor(min(energies)), math.ceil(max(energies))+1), fontsize=35)
    plt.yticks(fontsize=35)
    plt.savefig(f"Plots_Fits/y40cm_sigEvsE",facecolor='w')
    plt.clf()


def chi2_plots(ana_files, energies):
    fig, ax = plt.subplots(len(ana_files)+1,figsize=(10, 20))
    fig.suptitle(r'Reconstructed $\chi^2$', fontsize=13)
    mean_list = []
    std_list = []
    ### --- reduce x range around energy
    for i, file_path in enumerate(ana_files):
        file_ = uproot4.open(file_path)
        chi2 = file_['ce_emcal_calib;1/reco_chi2;1'].to_numpy()
        bin_centers = chi2[1][:-1] + np.diff(chi2[1]) / 2
        
        ### --- make histogram
        width = .85*(chi2[1][1] - chi2[1][0])
        ax[i].bar(chi2[1][:-1], chi2[0][0:], align='edge', width=width)

        ### --- labels
        title = r'%.2f GeV' % energies[i]
        ax[i].title(title)
        #ax[i].legend()
        #ax[i].xlabel("Energy")
        #ax[i].ylabel("Events")

    plt.savefig(f"Plots_Fits/plot_chi2",facecolor='w')


### --- Event display for pgun 
def event_display_gun(ana_files, output_files, section, energies):
    geo_file = "work/calib_gam_y100cm_9GeV_10000evt.geo.gdml"
    for i in range(len(ana_files)):
        fig, ax = plt.subplots(figsize=(30, 30))
        fig.suptitle(r'Event Display', fontsize=13)
        space = ax
        input_file = uproot4.open(ana_files[i])
        output_file = uproot4.open(output_files[i])

        geometry_xml = ET.parse(geo_file)
        pwo_size_x, pwo_size_y, pwo_size_z, unit = get_module_geometry('ce_EMCAL_detPWO', geometry_xml)
        pwo_pos_by_id = get_module_positions('ce_EMCAL_detPWO', geometry_xml,material='PWO')
        #print(f"PWO module size: size_x={pwo_size_x:.2f}{unit} size_y={pwo_size_y:.2f}{unit} size_z={pwo_size_z:>.2f}{unit}")
        #print(f"Total PWO modules: {len(pos_by_id)}")W

        lg_size_x, lg_size_y, lg_size_z, unit = get_module_geometry('ce_EMCAL_detGLASS', geometry_xml)
        lg_pos_by_id = get_module_positions('ce_EMCAL_detGLASS', geometry_xml,material='GLASS')
        #print(f"PWO module size: size_x={pwo_size_x:.2f}{unit} size_y={pwo_size_y:.2f}{unit} size_z={pwo_size_z:>.2f}{unit}")
        #print(f"Total PWO modules: {len(pos_by_id)}")
        
        ### --- Number of events to process:
        #95-5
        #110-5
        #120-5
        #170-10
        #300-10**
        #290-10*
        #310-10* 
        start_event=1
        process_events=1
        
        ### --- Plot hits
        if section == 'full':
            norm, cmap, _ = plot_calorimeter_hits_full(input_file, space, pwo_pos_by_id, pwo_size_x, pwo_size_y, start_event, material='PWO', process_events=process_events)
            norm, cmap, _ = plot_calorimeter_hits_full(input_file, space, lg_pos_by_id, lg_size_x, lg_size_y, start_event, material='GLASS', process_events=process_events)
        else:
            norm, cmap, _ = plot_calorimeter_hits_sections(input_file, output_file, space, pwo_pos_by_id, pwo_size_x, pwo_size_y, start_event, material='PWO',section=section, process_events=process_events)
            norm, cmap, _ = plot_calorimeter_hits_sections(input_file, output_file, space, lg_pos_by_id, lg_size_x, lg_size_y, start_event, material='GLASS', process_events=process_events)
        

        space.set_xbound(lower=-150, upper=150)
        space.set_ybound(lower=-120, upper=120)
        #space.axis('equal')
        #space.autoscale()

        ### --- save graph
        #title = r'%.2f GeV' % energies[i]
        #space.title(title)
        fig.colorbar(cm.ScalarMappable(norm=norm, cmap=cmap), ax=space)
        title = f"Plots_Fits/y40cm_{energies[i]}_event_display"
        plt.savefig(title, facecolor='w')
        plt.clf()

### --- Event display for generator
def event_display_gen(ana_files, output_file, section):#, energies):
    fig, ax = plt.subplots(figsize=(30, 30))
    fig.suptitle(r'Event Display', fontsize=13)
    file_path = ana_files[0]
    output_file = output_file[0]
    space = ax
    input_file = uproot4.open(file_path)
    output_file = uproot4.open(output_file)

    geo_file = f"work/calib_gam_y40cm_9GeV_10000evt.geo.gdml"
    geometry_xml = ET.parse(geo_file)
    pwo_size_x, pwo_size_y, pwo_size_z, unit = get_module_geometry('ce_EMCAL_detPWO', geometry_xml)
    pwo_pos_by_id = get_module_positions('ce_EMCAL_detPWO', geometry_xml,material='PWO')
    #print(f"PWO module size: size_x={pwo_size_x:.2f}{unit} size_y={pwo_size_y:.2f}{unit} size_z={pwo_size_z:>.2f}{unit}")
    #print(f"Total PWO modules: {len(pos_by_id)}")W

    lg_size_x, lg_size_y, lg_size_z, unit = get_module_geometry('ce_EMCAL_detGLASS', geometry_xml)
    lg_pos_by_id = get_module_positions('ce_EMCAL_detGLASS', geometry_xml,material='GLASS')
    #print(f"PWO module size: size_x={pwo_size_x:.2f}{unit} size_y={pwo_size_y:.2f}{unit} size_z={pwo_size_z:>.2f}{unit}")
    #print(f"Total PWO modules: {len(pos_by_id)}")
    
    ### --- Number of events to process:
    #95-5
    #110-5
    #120-5
    #170-10
    #300-10**
    #290-10*
    #310-10* 
    start_event=1
    process_events=1
    
    ### --- Plot hits
    if section == 'full':
        norm, cmap, _ = plot_calorimeter_hits_full(input_file, space, pwo_pos_by_id, pwo_size_x, pwo_size_y, start_event, material='PWO', process_events=process_events)
        norm, cmap, _ = plot_calorimeter_hits_full(input_file, space, lg_pos_by_id, lg_size_x, lg_size_y, start_event, material='GLASS', process_events=process_events)
    else:
        norm, cmap, _ = plot_calorimeter_hits_sections(input_file, output_file, space, pwo_pos_by_id, pwo_size_x, pwo_size_y, start_event, material='PWO',section=section, process_events=process_events)
        norm, cmap, _ = plot_calorimeter_hits_sections(input_file, output_file, space, lg_pos_by_id, lg_size_x, lg_size_y, start_event, material='GLASS', process_events=process_events)
    

    space.set_xbound(lower=-150, upper=150)
    space.set_ybound(lower=-120, upper=120)
    #space.axis('equal')
    #space.autoscale()

    ### --- save graph
    #title = r'%.2f GeV' % energies[i]
    #space.title(title)
    fig.colorbar(cm.ScalarMappable(norm=norm, cmap=cmap), ax=space)
    plt.savefig(f"Plots_Reco/y40cm__event_display_transition",facecolor='w')



'''
FOR GAUSSIAN FITS
x_interval_for_fit = np.linspace(reco_e[1][arg_min], reco_e[1][arg_max], 10000)
popt, _ = curve_fit(gaussian, reco_e[1][arg_min:arg_max], bin_centers[arg_min:arg_max], p0=[1., 0., energies[i]])
gauss = gaussian(x_interval_for_fit, *popt)
gauss_mean = np.mean(gauss)
gauss_std = np.std(gauss)
mean_list.append(gauss_mean)
std_list.append(gauss_std)
print("mean: ", round(gauss_mean,2))
print("std:  ", round(gauss_std, 2))

### plot fit
popt, _ = curve_fit(gaussian, bin_centers[arg_min:arg_max], reco_e[0][arg_min:arg_max], p0=[energies[i],max(reco_e[0]),1.])
gauss = gaussian(x_interval_for_fit, *popt)

### plot histogram
width = .85*(reco_e[1][1] - reco_e[1][0])
ax[i].bar(reco_e[1][arg_min:arg_max], reco_e[0][arg_min:arg_max], align='edge', width=width)
textstr = '\n'.join((
    r'$\mu=%.2f$' % (round(gauss_mean,2), ),
    r'$\sigma=%.2f$' % (round(gauss_std, 2), )))
# these are matplotlib.patch.Patch properties
props = dict(boxstyle='round', facecolor='wheat', alpha=0.5)
# place a text box in upper left in axes coords
ax[i].text(0.05, 0.95, textstr, transform=ax[i].transAxes, fontsize=14,
        verticalalignment='top', bbox=props)
ax[i].plot(x_interval_for_fit, gauss, label='fit', color="orange")
ax[i].legend()
ax[i].xlabel("Energy")
ax[i].ylabel("Events")'''